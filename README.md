




Neural networks for Granger Causality
=====================================

A series of neural network models that explicitly model dependence between multivariate time series. The code
provides optimization for each. The three models are: 

* MLP model with automatic lag selection. Uses proximal gradient ascent for sparsity.
* LSTM model. Also uses proximal gradient ascent for sparsity
* Multivariate Echo State network. Uses group lasso optimization for sparsity. Corrdinate ascent and group LARS currently implemeted for optimization.


* Nick to do:
  - MLP VAR synthetic experiments
    * Vary lag, dimensionality, and dimension
    * Figure out how to deal with Dream3 In Silico data
      https://www.synapse.org/#!Synapse:syn2853594/wiki/71567

