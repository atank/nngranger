\section{Background and problem formulation}
Let $x_t \in \mathbb{R}^p$ denote a $p$-dimensional stationary time series and assume we have observed the process at $T$ time points, $(x_1, \ldots, x_T)$. Granger causality (GC) in time series analysis is typically studied using the vector autoregressive model (VAR) \cite{lutkepohl:2005}. In this model, the time series $x_t$ is assumed to be a linear combination of the past $K$ lags of the series
\begin{align}
x_t = \sum_{k = 1}^K A^{(k)} x_{t - k} + e_t,
\end{align}
where $A^{(k)}$ is a $p \times p$ matrix that specifies how lag $k$ effects the future evolution of the series and $e_t$ is mean zero noise. In this model time series $j$ does not Granger cause time series $i$ iff $\forall k, A^{(k)}_{ij} = 0$. A Granger causal analysis in a VAR model thus reduces to determining which values in $A^{(k)}$ are zero over all lags. In higher dimensional settings, this may be determined by solving a group lasso regression problem
\begin{align} \label{linear}
\min_{A^{(1)},\ldots, A^{(K)}} \sum_{t = K}^T \left(x_t - \sum_{k = 1}^K A^{(k)} x_{t - k} \right)^2 \\
+ \lambda \sum_{ij} ||(A^{(1)}_{ij}, \ldots, A^{(K)}_{ij})||_2, \nonumber
\end{align}
where $||.||_2$ denotes the the $L_2$ norm which acts as a group penalty shrinking all values of $(A^{(1)}_{ij}, \ldots, A^{(K)}_{ij})$ to zero together \cite{yuan:2006} and $\lambda > 0$ is a tuning parameter that controls the level of group sparsity. More recently, \citep{nicholson:2014} replace the group penalty with a structured heirarchical penalty \cite{jenatton:2011,huang:2011} which, for a fixed $\lambda$ automatically selects the lag of each interaction.


\subsection{Nonlinear autoregressive models and Granger causality}
A \emph{nonlinear} autoregressive model allows $x_t$ to evolve according to more general nonlinear dynamics
\begin{align}
x_t &= g(x_{<t1},\dots, x_{<tp}) + e_t.
\end{align} 
where $x_{<ti} = \left({...,x_{(t - 2)i}, x_{(t-1)i}} \right)$ denotes the past of series $i$.
The nonlinear autoregressive function $g$ may be written componentwise:
\begin{align}
x_{ti} = g_{i}(x_{<t1}, \ldots, x_{<tp}) + \epsilon_{ti} \nonumber
\end{align}
where $g_i$ is a continuous function that specifies how the past $K$ lags influences series $i$. In this context, Granger non-causality  between two series $j$ and $i$ means that the function $g_{i}$ does not depend on $x_{<tj}$, the past lags of series $j$. More formally,
\begin{definition} \label{causal_def}
Time series $j$ is \emph{Granger non-causal} for time series $i$ if for all $(x_{<t1}, \ldots, x_{<tp})$ and all $x'_{<tj}$, 
\begin{align} 
g_{i}(x_{<t1}, \ldots, x_{<tj}, \ldots, x_{<tp}) = g_{i}(x_{<t1}, \ldots,x'_{<tj},\ldots x_{<tp}), \nonumber
\end{align}
that is, $g_i$ is invariant to $x_{<tj}$.
\end{definition}

Our goal is to flexibly estimate nonlinear Granger causal and non-causal relationships using a penalized optimization approach similar to Problem (\ref{linear}) for linear models.