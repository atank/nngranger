\section{Sparse Input MLPs for Time Series}
Our first approach models the nonlinear dynamics with a multilayer perceptron (MLP). In a forecasting setting, it is common to model the full set of outputs $x_t$ using an MLP where the inputs are $x_{<t} = x_{(t - 1):(t-K)}$, for some lag $K$. There are two problems with applying this approach to inferring GC. First, due to sharing of hidden layers, it is difficult to specify necessary conditions on the weights that simultaneously allows series $j$ to influence series $i$ but not influence series $i'$ for $i \neq i'$. Sufficient conditions for GC are needed because we wish to add selection penalties during estimation time. Second, a joint MLP requires all $g_i$ functions to depend on the same lags, however, in practice each $g_i$ may have different lag order dependencies.


To tackle these challenges we model each $g_i$ with a separate MLP, so that we can easily disentangle the effects from inputs to outputs. Assume that for each $i$, $g_i$ takes the form of an MLP with $L$ layers and let the vector $h^{\ell}_{t}$ denote the values of the $\ell$th hidden layer at time $t$.  Let ${\bf W} = \{W^{1}, \ldots, W^{L}\}$ denote the weights at each layer and let the first layer weights be written as $W^{1} = \{W^{11}, \ldots, W^{1K}\}$ . The hidden values at time $t$ are given by:
\begin{align} \label{mlp}
h^{\ell}_{t} = \sigma\left(\sum_{k = 1}^K W^{\ell k} x_{t - k} + b^{l}\right) 
\end{align} 
where $\sigma$ is an activation function and $b^{\ell}$ is the bias at layer $\ell$. The output, $x_{ti}$, is given by
\begin{align}
x_{ti} = w_{O}^{T} h^{L}_{t} + \epsilon_t.
\end{align}
where $w_{O}^{T}$ is the linear output decoder. In Eq.~\eqref{mlp}, if the $j$th column of the first layer weight matrix, $W^{1k}_{:j}$, is zero over all rows, then time series $j$ does not Granger cause series $i$. Thus, analogous to the VAR case, one may select for Granger causality by applying a group lasso penalty to the columns of the $W^{1k}$ matrices for each $g_i$ as in Eq.~\eqref{linear},
\begin{align} \label{gp_mlp}
\min_{{\bf W}} &\sum_{t = K}^{T} \left(x_{it} - g_i(x_{(t-1):(t - K)}\right)^2 \\
& + \lambda \sum_{j = 1}^p ||(W^{11}_{:j}, \ldots, W^{1K}_{:j})||_F. \nonumber
\end{align}
For large enough $\lambda$, the solutions to Eq.~\eqref{gp_mlp} will lead to many zero columns in each $W^{1k}$ matrix, implying only a small number of estimated Granger causal connections. 

The zero outgoing weights are sufficient but not necessary to represent Granger non-causality. Indeed, series $i$ could be Granger non-causal of series $j$ through a complex configuration of the weights that exactly cancel each other. However, since we wish to \emph{interpret} the outgoing weights of the inputs as a measure of dependence, it is important that these weights reflect the true relationship between inputs and outputs.  Our penalization scheme acts as a prior that biases the network to represent Granger non-causal relationships with zeros in the outgoing weights of the inputs, rather than through other configurations. Our simulation results in Section \ref{simulations} validate this intuition.

\begin{figure*}
\centering
\includegraphics[width = .6\textwidth]{mlpschem-crop.pdf}
\vspace{-.1in}
\caption{Schematic for modeling Granger causality using MLPs. (left) If the outgoing weights for series $j$, shown in dark blue, are penalized to zero, then series $j$ does not influence series $i$. (right) The group lasso penalty jointly penalizes the full set of outgoing weights while the hierarchical version penalizes the nested set of outgoing weights, penalizing higher lags more.}
\end{figure*}

\subsection{Simultaneous Granger causality and lag selection}
We may simultaneously select for Granger causality and select for the lag order of the interaction by adding a \emph{hierarchical} group lasso penalty \cite{nicholson:2014} to the MLP optimization problem,
\begin{align}\label{hg}
\min_{{\bf W}} &\sum_{i = 1}^{T} \left(x_{it} - g_i(x_{(t-1):(t - K)} \right)^2 \\
&+ \lambda \sum_{j = 1}^p \sum_{k = 1}^K ||(W^{1k}_{:j}, \ldots, W^{1K}_{:j} )||_F. \nonumber
\end{align}
The hierarchical penalty leads to solutions such that for each $j$ there exists a lag $k$ such that all $W^{1k'}_{:j} = 0$ for $k' > k$ and all $W^{1k'}_{:j} \neq 0$ for $k' \leq k$. Thus, this penalty effectively selects the lag of each interaction. The hierarchical penalty also sets many columns of $W^{1k}$ to be zero across all $k$, effectively selecting for Granger causality. In practice, the hierarchical penalty allows us to fix $K$ to a large value, ensuring that no Granger causal connections at higher lags are missed. 

We optimize Eq.~\eqref{hg} using a proximal gradient descent with line search and do not use minibatches. The proximal step for the group lasso penalty is given by a group soft thresholding operation on the input weights~\cite{parikh:2014}. The proximal step for the heirarchical penalty is given by iteratively applying the group soft-thresholding operation on each nested group in the penalty, from the smallest group to the largest group~\cite{jenatton:2011}.

\section{Sparse Input Recurrent Neural Networks}

Recurrent neural networks (RNNs) are particularly well suited to modeling time series, as they compress the past of a time series into a hidden state, allowing them to capture complicated nonlinear dependencies at longer time lags than traditional time series models. Like MLPs, time series forecasting with RNNs typically proceeds by jointly modeling the entire evolution of the multivariate series using a single recurrent network. However, as in the MLP case, it is difficult to disentangle how each series effects the evolution of another series. This problem is even more severe in complicated recurrent networks like LSTMs. 

To model Granger causality with RNNs, we instead follow the same strategy as with MLPs and model each $g_i$ function using a separate RNN. For simplicity, we assume a one layer recurrent network, but our formulation may be easily modified to accommodate more layers.

\subsection{Componentwise Recurrent Networks}
Let \todo{scalar?}{$h_{t - 1} \in \mathbb{R}$} represent the hidden state at time $t$, that represents the historical context of the time series for predicting a component $x_{ti}$. The hidden state at time $t+1$ is updated recursively
\begin{align}
h_t = f(x_{t},h_{t - 1}),
\end{align}
where $f$ is some nonlinear function that depends on the particular recurrent architecture. For simplicity, we model the output, $g_i(x_{<t})$, as a linear function of the hidden states at time $t$:
\begin{align}
x_{it} &= g_i(x_{<t}) + \epsilon_t = w_{O}^{T} h_t + \epsilon_t,
\end{align}
where the dependence of $g_i$ on the full past sequence $x_{<t}$ is due to recursive updates of the hidden state $h_t$. 

Due to their effectiveness at modeling a wide variety of dependent data, we choose to model the recurrent function $f$ using a LSTM. The LSTM model introduces a second hidden state variable $c_t$, reffered to as the cell state, giving the full set of hidden parameter as  $(c_t, h_t)$. The LSTM model takes the following typical form
\begin{align}
f_{t} &= \sigma \left(W^f x_{t} +  U^f h_{(t-1)} \right) \nonumber\\
i_{t} &= \sigma \left(W^{in} x_{t} + U^{in} h_{(t-1)} \right) \nonumber \\
o_{t} &= \sigma \left(W^o x_{t} + U^o h_{(t-1)i} \right) \nonumber \\
c_{t} &= f_{t} \odot c_{t-1} + i_{t} \odot \sigma \left(W^c x_{t} + U^c h_{(t-1)} \right) \nonumber \\
h_{t} &= o_{t} \odot \sigma(c_{t})  \nonumber 
\end{align}
where $\odot$ denotes componentwise multiplication and $i_t$, $f_t$, and $o_t$ represent input, forget and output gates, respectively, that control how each component of the state cell, $c_t$, is updated and then transferred to the hidden state used for prediction, $h_t$. 

\subsection{Granger causality selection in LSTMs}
Let $W =  \left((W^{f})^T, (W^{in})^T, (W^{c})^T \right)^T$ and $U = \left((U^{f})^T, (U^{in})^T, (U^{c})^T \right)^T$ be the vertical concatenation of all the input matrices and hidden state matrices of the LSTM, respectively. Like in the MLP case, for this componentwise LSTM model a sufficient condition for Granger non-causality of an input series $j$ on an output $i$ is that all elements of the $j$th row of $W$ are zero, $W_{j:} = 0$. Thus we may select for which series Granger cause series $i$ during estimation using a \todo{but you just said rows}{group lasso penalty across columns of $W$}

\begin{align}\label{lstm_group}
\min_{ W, U, w^O} &\sum_{t = 2}^{T} \left(x_{it} - g_i(x_{<t}\right))^2 \\
&+ \lambda \sum_{j = 1}^p ||W_{:j}||_2. \nonumber
\end{align}

For a large enough $\lambda$ many columns of $W$ will be zero, thus leading to a sparse set of Granger causal connectivity.

Similar to the MLP, we optimize Eq.~\eqref{lstm_group} using proximal gradient ascent with line search. When the data consists of many replicates of short time series, like in the DREAM data in Section~\ref, we perform a full backpropagation through time~\cite{} to compute the gradients. However, for longer series we truncate the backpropagation through time by unlinking the hidden sequences~\cite{}. In these models the gradients used to optimize Eq.~\eqref{lstm_group} are only approximations of the gradients of the full component-wise LSTM model\footnote{Code for all models can be found at \texttt{http://github.com/...}}.


\subsection{Comparing MLP and recurrent models for Granger causality}
Both MLP and LSTM frameworks model each output $x_{it} = g_i(x_{<t})$ using independent networks for each $i$. For the MLP model one needs to specify a maximum possible model lag $K$, however our lag selection strategy allows one to set that to a large value and the weights for higher lags are automatically removed from the model. On the other hand, the LSTM model requires no maximum lag specification, and instead automatically learns the memory of each interaction. As a consequence, the MLP and LSTM differ in the amount of data used for training, as noted by a comparison of the $t$ index in Eq.~\eqref{lstm_group} and \eqref{hg}. For a length $T$ series, the MLP model only uses $T - K$ data points, however the LSTM model always uses $T - 1$ data points. For large $T$ this difference is not significant, however when the data consists of many replicates of short series, as in the DREAM3 data in Sec.~\ref{}, the difference may be significant. For example, for $M$ replicates, the LSTM model has $M(K-1)$ more training data than the MLP. {\bf point to how LSTM does better on DREAM}.

