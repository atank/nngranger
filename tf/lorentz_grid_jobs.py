from itertools import product
import numpy as np
import time

dstamp = time.strftime('%Y%m%d')
tstamp = time.strftime('%H%M%S')

T_grid = [1e4]
p_grid = [10]

lam_grid = np.logspace(-5.0, -2.0, num = 50)
lr_grid = 10.0 ** np.arange(-4, -1)
nepochs_grid = [20000]
pen_types = ['group_lasso']
opt_types = ['momentum']
arch_grid = [1]

baseseed = 8675309
num_random = 10
nrestarts = 5

param_grid = product(T_grid, p_grid, lam_grid, nepochs_grid, pen_types, opt_types, arch_grid)

jobname = 'lorentz_experiment_%s_%s' % (dstamp, tstamp)
jobfile = '%s.job' % jobname

BASECMD = 'python init_lorentz_experiment.py'

OUTDIR = './out/%s' % jobname
CHKDIR = './checkpoints/%s' % jobname

with open(jobfile, 'w') as f:
	for param in param_grid:
		T, p, lam, nepochs, pen_type, opt_type, arch = param

		network_lag = 3

		for s in range(num_random):
			argstr = ' --T=%d --p=%d '
			argstr += ' --network_lag=%d --lam=%e --nepoch=%d'
			argstr += ' --penalty_type=%s --opt_type=%s --seed=%d'
			argstr += ' --outdir=%s --chkpointdir=%s'
			argstr += ''.join([' --lr=%e' % lr for lr in lr_grid])
			argstr += ' --arch=%d'
			argstr += ' --nrestarts=%d'
			argstr = argstr % (T, p, network_lag, lam, nepochs, pen_type, opt_type, baseseed + s, OUTDIR, CHKDIR, arch, nrestarts)
			f.write(BASECMD + argstr + '\n')

