import argparse
import pickle
import os
import sys
import numpy as np
from itertools import product
import shutil

from generate_data import generate_data_general
from run_mlp_experiment import run_experiment

# Prepare argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--T', type=int, default=1000,
                    help='length of generated time series')
parser.add_argument('--arch', type=int, default=1,
                    help='network architecture')
parser.add_argument('--p', type=int, default=10,
                    help='number of time series')
#parser.add_argument('--lag', type=int, default=1,
#                    help='lag of generated data')
#parser.add_argument('--sparsity', type=float, default=0.3,
#                    help='sparsity fraction of generated data')
parser.add_argument('--network_lag', type=int, default=1,
                    help='maximum lags of model')
parser.add_argument('--lam', type=float, default=1.0,
                    help='regularization parameter for weight penalty')
parser.add_argument('--lr', type=float, action='append',
                    help='learning rate')
parser.add_argument('--nepoch', type=int, default=1000,
                    help='number of epochs')
parser.add_argument('--mbsize', type=int, default=None,
                    help='number of time points in minibatch')
parser.add_argument('--penalty_type', type=str, default='group_lasso',
                    help='type of weight penalty. [group_lasso, h_truncation]')
parser.add_argument('--opt_type', type=str, default='prox',
                    help='type of optimization [prox, adam, momentum]')
parser.add_argument('--seed', type=int, default=8675309,
                    help='random seed')
parser.add_argument('--nrestarts', type=int, default=1,
                    help='number of random restarts for optimization')
parser.add_argument('--outdir', type=str, default="./out/var_experiment",
                    help='directory to store results')
parser.add_argument('--chkpointdir', type=str, default="./checkpoints/exp-mlp-var",
                    help='directory to store checkpoint files')
#parser.add_argument('--verbose', type=int, default=0,
#                    help='verbosity level')

# Extract command line arguments
args = parser.parse_args()

T = args.T
p = args.p
#lag = args.lag
#sparsity = args.sparsity
network_lag = args.network_lag
lam = args.lam
penalty_type = args.penalty_type
opt_type = args.opt_type
seed = args.seed
nrestarts = args.nrestarts
nepoch = args.nepoch
mbsize = args.mbsize
arch = args.arch
lr_list = args.lr

OUTDIR = args.outdir
chkpointdir = args.chkpointdir

# Create file name
if mbsize is None:
	batchsize = 0
else:
	batchsize = mbsize

experiment_name = 'lorentz_experiment_'
experiment_name += 'T-%d_p-%d_networklag-%d_lam-%e_'
experiment_name += '_pentype-%s_opt-%s_nepoc-%d_mbsz-%d_seed-%d'
experiment_name = experiment_name % (T, p, network_lag, lam, penalty_type, opt_type, nepoch, batchsize, seed)

out_name = os.path.join(OUTDIR, experiment_name + '.data')

# Ensure that experiment has not already been performed
if os.path.isfile(out_name):
	print('Experiment has already been performed')
	sys.exit()

# Create OUTDIR, if it doesn't exist
if not os.path.exists(OUTDIR):
	os.makedirs(OUTDIR)

# Create checkpoint dir, if it doesn't exist
if not os.path.exists(os.path.join(chkpointdir, experiment_name)):
	os.makedirs(os.path.join(chkpointdir, experiment_name))

# Determine architecture
if arch == 1:
	hidden_units = [p]
elif arch == 2:
	hidden_units = [p, p]
elif arch == 3:
	hidden_units = [2 * p]
elif arch == 4:
	hidden_units = [2 * p, 2 * p]
else:
	raise ValueError('arch must be in {1, 2, 3, 4}')

# Generate Lorentz data
data_specs = {
	'p': p,
	'forcing_constant': 5,
	'delta_t': 0.01,
	'sd': 0.1,
	'noise_add': 'global',
	'network_lag': network_lag
}

X_train, Y_train, X_test, Y_test, GC_actual = generate_data_general('lorentz', 0.1, T, data_specs)

# Prepare containers for best results
best_lr = [None] * p
best_train_loss = [None] * p
best_test_loss = [np.inf] * p
best_weights = [None] * p

# Set seed (for network initialization)
np.random.seed(seed)

# Run optimizations
for lr, restart in product(lr_list, range(nrestarts)):

	# Create checkpoint filename
	chk_file = os.path.join(chkpointdir, experiment_name, '_chk-%e-%d.data' % (lr, restart))

	# See if checkpoint file already exists
	if os.path.isfile(chk_file):
		print('Loading results for lr=%e, restart=%d' % (lr, restart))

		with open(chk_file, 'rb') as f:
			results = pickle.load(f)
		test_loss = results['test_loss']
		train_loss = results['train_loss']
		weights_list = results['weights_list']

	else:
		print('Running experiment for lr=%e, restart=%d' % (lr, restart))

		# Run experiment
		test_loss, train_loss, weights_list = run_experiment(X_train, Y_train, X_test, Y_test, network_lag, nepoch, lr, lam, penalty_type, hidden_units, opt_type, mbsize = mbsize)

		# Save results
		results = {
			'test_loss': test_loss,
			'train_loss': train_loss,
			'weights_list': weights_list
		}

		with open(chk_file, 'wb') as f:
			pickle.dump(results, f)
	
	# Check if these are best results yet
	for target in range(p):
		if test_loss[-1, target] < best_test_loss[target]:
			best_test_loss[target] = test_loss[-1, target]
			best_train_loss[target] = train_loss
			best_weights[target] = weights_list[target]
			best_lr[target] = lr

# Create GC estimate grid
GC_est = np.zeros((p, p))
for target in range(p):
	W = weights_list[target]
	for j in range(p):
		start = j * network_lag
		end = (j + 1) * network_lag
		GC_est[target, j] = np.linalg.norm(W[start:end, :], ord = 2)

# Format results
experiment_params = {
	'T': T,
	'p': p,
	'seed': seed
}

training_params = {
	'network_lag': network_lag, 
	'lam': lam, 
	'lr': lr_list,
	'nrestarts': nrestarts,
	'penalty_type': penalty_type, 
	'opt_type': opt_type,
	'nepoch': nepoch, 
	'mbsize': mbsize
}

out_dic = {
	'weights': weights_list,
	'train_loss': best_train_loss,
	'test_loss': best_test_loss,
	'GC_actual': GC_actual,
	'GC_est': GC_est,
	'experiment_params': experiment_params,
	'training_params': training_params
}

# Save results
with open(out_name, 'wb') as f:
	pickle.dump(out_dic, f, protocol = pickle.HIGHEST_PROTOCOL)

# Delete checkpoint files
shutil.rmtree(os.path.join(chkpointdir, experiment_name))


