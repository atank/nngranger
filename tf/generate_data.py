from __future__ import division
import numpy as np

def lorentz_96(forcing_constant, p, N, delta_t = 0.01, sd = 0.1, noise_add = 'global'):
	burnin = 4000
	N += burnin
	z = np.zeros((N, p))
	z[0, :] = np.random.normal(loc = 0, scale = 0.01, size = p)
	for t in range(1, N):
		for i in range(p):
			upone = (i + 1) % p
			grad = (z[t - 1, upone] - z[t - 1, i - 2]) * z[t - 1, i - 1] - z[t - 1, i] + forcing_constant
			z[t, i] = delta_t * grad + z[t - 1, i]
			if noise_add == 'step':
				z[t, i] += np.random.normal(loc = 0, scale = sd, size = 1)

	if noise_add == 'global':
		z += np.random.normal(loc = 0, scale = sd, size = (N, p))

	GC_on = np.zeros((p, p))
	for i in range(p):
		GC_on[i, i] = 1
		GC_on[i, (i - 1) % p] = 1
		GC_on[i, (i - 2) % p] = 1

	return z[range(burnin, N), :].T, GC_on

# TODO doesn't return GC grid
def gen_chaotic_lotka_volterra(T):
	""" Generate three node chaotic Lotka-Volterra dynamics.

		From "Supplementary information for detecting causality from nonlinear
		dynamcis with short-term time series"

		T : int
		length of generated series

		Returns:

		Y : 3 x T ndarray
		generated time series
	"""
	Ttrans = 100
	X = np.zeros([3, T+Ttrans])

	X[:,0] = np.random.rand(3)

	for t in xrange(1,T+Ttrans):
		X[0,t] = X[0,t-1]*(2. - X[0,t-1] + 0.675*X[1,t-1] - 0.5*X[2,t-1])
		X[1,t] = X[1,t-1]*(2. - 0.5*X[0,t-1] - X[1,t-1] + 0.675*X[2,t-1])
		X[2,t] = X[2,t-1]*(2. + 0.675*X[0,t-1] - 0.5*X[1,t-1] - X[2,t-1])

	return np.require(X[:,:T], requirements='C')

def gen_nonlinear_three_node(T, mode='fanout'):
	""" Generate three node nonlinear dynamics.

		From "Supplementary information for detecting causality from nonlinear
		dynamics with short-term time series"

		T : int
		length of generated series

		mode : str
		'fanout' or 'fanin'

		Returns:

		Y : 3 x T ndarray
		generated time series

		GC_on : 3 x 3 ndarray
	"""
	Ttrans = 100
	Y = np.zeros([3, T+Ttrans])

	if mode == 'fanout':
		gamma = np.array([[4., 0., 0.,], [0.21, 3.1, 0.], [-0.636, 0., 2.12]])
	elif mode == 'fanin':
		gamma = np.array([[4., 0., 0.,], [0., 3.6, 0.], [0.636, -0.636, 2.12]])

	GC_on = (np.abs(gamma) > 0).astype(int)
	Y[:,0] = np.random.rand(3)

	gamma_d = np.diag(gamma)

	for t in range(1,T+Ttrans):
		Y[:,t] = Y[:,t-1]*(gamma_d - np.dot(gamma, Y[:,t-1]))
		#for j in range(3):
		#    Y[j,t] = Y[j,t-1]*(gamma_d[j] - np.dot(gamma[j,:], Y[:,t-1]))

	return np.require(Y[:,:T].T, requirements = 'C'), GC_on

def linear_model(sparsity, p, sd_beta, sd_x, sd_e, N):
	""" Generate data from a linear model (without time series dynamics).

		sparsity : float

		p : int

		sd_beta : float

		sd_x : float

		sd_e : float

		N : int

		Returns:

		Y : length N ndarray
		time series for response variable

		X : N x p ndarray
		time series for predictor variables

		GC_on : length p ndarray
		binary vector representing which predictors affect response
	"""
	# Generate beta vector
	on = int(np.floor(sparsity * p))
	GC_on = np.concatenate((np.zeros(p - on), np.ones(on)), axis = 0)
	beta = np.random.normal(loc = 0, scale = sd_beta, size = on)
	min_effect = 1
	beta[(beta < min_effect) & (beta > 0)] = min_effect
	beta[(beta > - min_effect) & (beta < 0)] = - min_effect
	beta = np.concatenate((np.zeros(p - on), beta), 0)

	# Generate X and Y
	X = np.random.normal(loc = 0, scale = sd_x, size = (N, p))
	Y = np.dot(X, beta) + np.random.normal(loc = 0, scale = sd_e, size = N)
	return Y, X, GC_on

def stationary_var(beta,p,lag,radius):
	bottom = np.hstack((np.eye(p * (lag-1)), np.zeros((p * (lag - 1), p))))  
	beta_tilde = np.vstack((beta,bottom))
	eig = np.linalg.eigvals(beta_tilde)
	maxeig = max(np.absolute(eig))
	not_stationary = maxeig >= radius
	return beta * 0.95, not_stationary

def var_model(sparsity, p, sd_beta, sd_e, N, lag):
	radius = 0.97
	min_effect = 1
	beta = np.random.normal(loc = 0, scale = sd_beta, size = (p, p * lag))
	beta[(beta < min_effect) & (beta > 0)] = min_effect
	beta[(beta > - min_effect) & (beta < 0)] = - min_effect

	GC_on = np.random.binomial(n = 1, p = sparsity, size = (p, p))
	for i in range(p):
		beta[i, i] = min_effect # ?
		GC_on[i, i] = 1

	GC_lag = GC_on
	for i in range(lag - 1):
		GC_lag = np.hstack((GC_lag, GC_on))

	beta = np.multiply(GC_lag, beta)
	errors = np.random.normal(loc = 0, scale = sd_e, size = (p, N))

	# 1) Why does it need to be stationary?
	# 2) How does this work?
	not_stationary = True
	while not_stationary:
		beta, not_stationary = stationary_var(beta, p, lag, radius)

	X = np.zeros((p, N))
	X[:, range(lag)] = errors[:, range(lag)]
	for i in range(lag, N):
		X[:, i] = np.dot(beta, X[:, range(i - lag, i)].flatten(order = 'F')) + errors[:, i]

	return X, beta, GC_on

def format_ts_data(X, lag, target_index, testing_portion = 0.1):
	""" Format multivariate time series for convenient usage with MLP

		X : p x N ndarray

		lag : int

		target_index : int
		index of response variable in X

		testing_portion : float
		parameter for splitting data into training and testing

		Returns:

		X_train : N_train x (p * lag) ndarray 

		Y_train : N_train x p ndarray

		X_test : N_test x (p * lag) ndarray

		Y_test : N_test x p ndarray

	"""
	p, N = X.shape
	N_new = N - lag
	X_out = np.zeros((p * lag, N_new))
	Y_out = np.zeros(N_new)
	for i in range(lag, N):
		X_out[:, i - lag] = X[:, range(i - lag, i)].flatten(order = 'C')
		Y_out[i - lag] = X[target_index, i]

	N_test = int(N_new * testing_portion)
	N_train = N_new - N_test

	X_train = X_out[:, range(N_train)]
	X_test = X_out[:, range(N_train, N_new)]
	Y_train = Y_out[range(N_train)]
	Y_test = Y_out[range(N_train, N_new)]

	return X_train.T, Y_train, X_test.T, Y_test

def format_ts_data_multiple(X, lag, testing_portion = 0.1):
	""" Format multivariate time series for convenient usage with MLP

		X : p x N ndarray

		lag : int

		testing_portion : float

		Returns:

		X_train : N_train x (p * lag) ndarray 

		Y_train : N_train x p ndarray

		X_test : N_test x (p * lag) ndarray

		Y_test : N_test x p ndarray

	"""
	p, N = X.shape
	N_new = N - lag
	X_out = np.zeros((p * lag, N_new))
	Y_out = np.zeros((p, N_new))

	for i in range(lag, N):
		X_out[:, i - lag] = X[:, range(i - lag, i)].flatten(order = 'C')
		Y_out[:, i - lag] = X[:, i]

	N_test = int(N_new * testing_portion)
	N_train = N_new - N_test

	X_train = X_out[:, range(N_train)]
	X_test = X_out[:, range(N_train, N_new)]
	Y_train = Y_out[:, range(N_train)]
	Y_test = Y_out[:, range(N_train, N_new)]

	return X_train.T, Y_train.T, X_test.T, Y_test.T

def generate_data_general(model, testing_portion, N, data_specs, seed = 12345):
	np.random.seed(seed)
	
	if model == 'VAR':
		sparsity = data_specs['sparsity']
		p = data_specs['p']
		sd_beta = data_specs['sd_beta']
		sd_e = data_specs['sd_e']
		lag = data_specs['lag']
		X, beta, GC_on = var_model(sparsity, p, sd_beta, sd_e, N, lag)

		network_lag = data_specs['network_lag']
		X_train, Y_train, X_test, Y_test = format_ts_data_multiple(X, network_lag, testing_portion)

	elif model == 'linear':
		sparsity = data_specs['sparsity']
		p = data_specs['p']
		sd_beta = data_specs['sd_beta']
		sd_e = data_specs['sd_e']
		sd_x = data_specs['sd_x']
		Y, X, GC_on = linear_model(sparsity, p, sd_beta, sd_x, sd_e, N)

		N_test = int(N * testing_portion)
		N_train = N - N_test
		X_train = X[:N_train, :]
		Y_train = Y[:N_train]
		X_test = X[N_train:, :]
		Y_test = Y[N_train:]

	elif model == 'lorentz':
		p = data_specs['p']
		forcing_constant = data_specs['forcing_constant']
		delta_t = data_specs['delta_t']
		sd = data_specs['sd']
		noise_add = data_specs['noise_add']
		Z, GC_on = lorentz_96(forcing_constant, p, N, delta_t = delta_t, sd = sd, noise_add = noise_add)

		network_lag = data_specs['network_lag']
		X_train, Y_train, X_test, Y_test = format_ts_data_multiple(Z, network_lag, testing_portion)

	elif model == 'nonlinear_three_node':
		mode = data_specs['mode']
		X, GC_on = gen_nonlinear_three_node(N, mode)

		network_lag = data_specs['network_lag']
		X_train, Y_train, X_test, Y_test = format_ts_data_multiple(X, network_lag, testing_portion)

	else:
		return None

	return X_train, Y_train, X_test, Y_test, GC_on