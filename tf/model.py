import tensorflow as tf
import numpy as np
import time

''' Functions for creating neural network architecture '''

def parallel_model_architecture(p, network_lag, hidden_units, lam, penalty_type, learning_rate, opt):
	''' Create TF NN, including input and output placeholders, network architecture,
		full network loss function, individual network loss functions, and optimizer
	'''
	# Create placeholders
	x = tf.placeholder(tf.float32, shape = (None, p * network_lag))
	y_list = []
	for i in range(p):
		y_list.append(tf.placeholder(tf.float32, shape = (None, 1)))

	# Get y_pred's, W's
	W_list = []
	y_pred_list = []
	for i in range(p):
		y_pred, W = create_network_with_placeholders(p * network_lag, hidden_units, x)
		W_list.append(W)
		y_pred_list.append(y_pred)

	# Set up loss function and optimizer
	optimizer = optimizerWrapper(p, y_pred_list, y_list, W_list, penalty_type, network_lag, lam, learning_rate, opt)

	return x, y_list, optimizer, W_list

def create_network_with_placeholders(input_size, hidden_units, x):
	''' Helper function that creates an individual network's architecture
	'''
	# Initial hidden layer
	h = hidden_units[0]
	W = tf.Variable(tf.random_normal([input_size, h], mean = 0, stddev = 1))
	b = tf.Variable(tf.random_normal([h], mean = 0, stddev = 1))

	layer = tf.nn.sigmoid(tf.add(tf.matmul(x, W), b))

	# Subsequent hidden layers
	for i in range(1, len(hidden_units)):
		W_next = tf.Variable(tf.random_normal([hidden_units[i - 1], hidden_units[i]], mean = 0, stddev = 1))
		b_next = tf.Variable(tf.random_normal([hidden_units[i]], mean = 0, stddev = 1))

		layer = tf.nn.sigmoid(tf.add(tf.matmul(layer, W_next), b))

	# Output layer
	h_last = hidden_units[-1]
	W_final = tf.Variable(tf.random_normal([h_last, 1], mean = 0, stddev = 1))
	b_final = tf.Variable(tf.random_normal([1], mean = 0, stddev = 1))
	y_pred = tf.add(tf.matmul(layer, W_final), b_final)

	return y_pred, W


''' Class for handling optimization '''

class optimizerWrapper:
	''' Wrapper class that handles optimization of TF NNs. Used to provide
		experiments with consistent API for optimizer iterations, despite 
		possibility of using a) built in optimizer (such as Adam) or b)
		custom proximal updates

		self.run_optimizer is the function used for training iterations
	'''
	def __init__(self, p, y_pred_list, y_list, W_list, penalty_type, network_lag, lam, learning_rate, opt):
		# Set up various loss terms
		loss_list = []
		squared_error_list = []
		for i in range(p):
			squared_error = tf.reduce_mean(tf.pow(y_list[i] - y_pred_list[i], 2))
			penalty = apply_penalty(W_list[i], lam, penalty_type, p, network_lag)
			loss = squared_error + penalty
			squared_error_list.append(squared_error)
			loss_list.append(loss)

		self.loss_list = loss_list
		self.opt = opt
		self.learning_rate = learning_rate

		# Set up optimization procedure
		if opt == 'prox':			
			full_loss = sum(squared_error_list)
			self.loss_components = np.array(squared_error_list)

			# Save parameters (only used in prox case)
			self.W_list = W_list
			self.p = p
			self.network_lag = network_lag
			self.lam = lam

			# Designate proximal update routine
			if penalty_type == 'group_lasso':
				self.proximal_update = proximal_group_lasso
			elif penalty_type == 'hierarchical':
				self.proximal_update = proximal_hierarchical
			else:
				raise ValueError('penalty_type must be in [group_lasso, hierarchical]')

			# Set built in optimizer, used in run_optimizer
			self.optimizer = tf.train.MomentumOptimizer(learning_rate, momentum = 0.9, use_locking = False).minimize(full_loss)
			
			# Set training function
			self.run_optimizer = self._run_prox

		else:
			full_loss = sum(loss_list)
			self.loss_components = np.array(loss_list)

			# Set built in optimizer, used in run_optimizer
			if opt == 'adam':
				self.optimizer = tf.train.AdamOptimizer(learning_rate, beta1 = 0.9, beta2 = 0.999, epsilon = 1e-08, use_locking = False).minimize(full_loss)
			elif opt == 'momentum':
				self.optimizer = tf.train.MomentumOptimizer(learning_rate, momentum = 0.9, use_locking = False).minimize(full_loss)
			
			# Set training function
			self.run_optimizer = self._run_builtin
		

	def _run_prox(self, sess, feed_dict = None):
		''' For running training iteration using proximal updates '''
		self.optimizer.run(feed_dict = feed_dict)
		#start = time.time()
		self.proximal_update(self.W_list, self.lam, self.p, self.network_lag, sess)
		#end = time.time()
		#print('Prox update took %f' % (end - start))

	def _run_builtin(self, sess, feed_dict = None):
		''' For running training iteration using built in optimizer '''
		self.optimizer.run(feed_dict = feed_dict)

	def compute_loss(self, sess, target = None, feed_dict = None):
		''' Optimizer wrapper contains individual networks' loss function, and
			provides interface for computing loss after each epoch. (Note that
			when using proximal updates, sum off loss functions is not what TF
			built in optimizer attempts to optimize)
		'''
		if target is None:
			return sess.run(self.loss_list, feed_dict = feed_dict)
		else:
			return sess.run(self.loss_list[target], feed_dict = feed_dict)

	def deactivate_series(self, sess, on_vector):
		full_loss = np.sum(self.loss_components[on_vector])

		if self.opt == 'prox':			
			full_loss = sum(squared_error_list)
			self.loss_components = np.array(squared_error_list)

			# Save parameters (only used in prox case)
			self.W_list = W_list
			self.p = p
			self.network_lag = network_lag
			self.lam = lam

			# Set built in optimizer, used in run_optimizer
			self.optimizer = tf.train.MomentumOptimizer(self.learning_rate, momentum = 0.9, use_locking = False).minimize(full_loss)

			# TODO prox gradient descent not compatible with feature of turning off series

		else:
			# Set built in optimizer, used in run_optimizer
			if self.opt == 'adam':
				opt = tf.train.AdamOptimizer(self.learning_rate, beta1 = 0.9, beta2 = 0.999, epsilon = 1e-08, use_locking = False)
				self.optimizer = opt.minimize(full_loss)
				# TODO Adam not compatible with feature of turning off series

			elif self.opt == 'momentum':
				opt = tf.train.MomentumOptimizer(self.learning_rate, momentum = 0.9, use_locking = False)
				self.optimizer = opt.minimize(full_loss)
				var_list = [var for var in tf.global_variables() if 'Momentum' in var.name]
				sess.run(tf.variables_initializer(var_list))


''' Functions for handling regularization when performing proximal updates '''

def vectorized_group_lasso_update(weights, lam, p, network_lag):
	''' Performs group lasso proximal update in vectorized way
	'''
	h = weights.shape[1]
	vectorized_weights = np.reshape(weights, newshape = (p, network_lag * h), order = 'C')
	norm_value = np.linalg.norm(vectorized_weights, axis = 1, ord = 2) 
	norm_value_gt = norm_value >= lam
	vectorized_weights[np.logical_not(norm_value_gt), :] = 0
	non_zero = vectorized_weights[norm_value_gt, :]
	vectorized_weights[norm_value_gt] = vectorized_weights[norm_value_gt] * (lam / norm_value[norm_value_gt][np.newaxis].T)

def proximal_group_lasso(W_list, lam, p, network_lag, sess):
	''' Sets TF Variables' weights by making proximal updates for each 
		group Lasso term 
	'''
	W_list_values = sess.run(W_list)
	#assignment_list = []
	for i in range(p):
		values = W_list_values[i]
		vectorized_group_lasso_update(values, lam, p, network_lag)
		#assignment_list.append(W_list[i].assign(values, use_locking = False))
		#start = time.time()
		W_list[i].assign(values, use_locking = False).eval()
		#end = time.time()
		#print('\t\t W write took %f' % (end - start) * p)

	#start = time.time()
	#sess.run(assignment_list)
	#end = time.time()
	#print('\t\t W write took %f' % (end - start))

def vectorized_hierarchical_lasso_update(weights, lam, p, network_lag):
	h = weights.shape[1]
	vectorized_weights = np.reshape(weights, newshape = (p, network_lag * h), order = 'C')
	for i in range(1, network_lag + 1):
		start = 0
		end = h * i
		norm_value = np.linalg.norm(vectorized_weights[:, start:end], axis = 1, ord = 2)
		norm_value_gt = norm_value >= lam
		vectorized_weights[np.logical_not(norm_value_gt), start:end] = 0
		vectorized_weights[norm_value_gt, start:end] = vectorized_weights[norm_value_gt, start:end] * (lam / norm_value[norm_value_gt][np.newaxis].T)

def proximal_hierarchical(W_list, lam, p, network_lag, sess):
	''' Sets TF Variables' weights by making proximal updates for each
		hierarchical group lasso term
	'''
	W_list_values = sess.run(W_list)
	for i in range(p):
		values = W_list_values[0]
		vectorized_hierarchical_lasso_update(values, lam, p, network_lag)
		W_list[i].assign(values, use_locking = False).eval()


''' Functions for handling regularization when relying solely on built-in optimizer '''

def group_lasso(weights, lam, p, network_lag):
	loss_list = []
	for i in range(p):
		start = i * network_lag
		end = (i + 1) * network_lag
		loss_list.append(tf.norm(weights[start:end, :], ord = 'euclidean'))
	penalty = sum(loss_list)
	return tf.multiply(penalty, tf.constant(lam, dtype = tf.float32))

def hierarchical_loss(weights, lam, p, network_lag):
	loss_list = []
	for i in range(p):
		start = i * network_lag
		end = (i + 1) * network_lag
		for j in range(network_lag):
			loss_list.append(tf.norm(weights[start:(end - j), :], ord = 'euclidean'))
	penalty = sum(loss_list)
	return tf.multiply(penalty, tf.constant(lam, dtype = tf.float32))

def apply_penalty(weights, lam, penalty_type, p, network_lag):
	''' Interface for experiments to apply specified penalty '''
	if penalty_type == 'hierarchical':
		return hierarchical_loss(weights, lam, p, network_lag)
	elif penalty_type == 'group_lasso':
		return group_lasso(weights, lam, p, network_lag)
	else:
		raise ValueError('penalty_type must be in [group_lasso, hierarchical]')