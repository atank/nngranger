import tensorflow as tf
import numpy as np
from math import ceil

from model import parallel_model_architecture, optimizerWrapper

def run_experiment(X_train, Y_train, X_test, Y_test, lag, epochs, learning_rate, lam, penalty_type, hidden_units, opt, mbsize = None, verbose = True, loss_step = 100):
	# Get data size
	p = Y_train.shape[1]
	N_train = X_train.shape[0]
	N_test = X_test.shape[0]

	# Batch parameters
	batched = not mbsize is None
	if batched:
		n_batches = ceil(N_train / mbsize)

	# MLP model
	tf.reset_default_graph()
	x, y_list, optimizer, W_list = parallel_model_architecture(p, lag, hidden_units, lam, penalty_type, learning_rate, opt)

	# Initialize and begin training
	train_loss = np.zeros((epochs, p))
	test_loss = np.zeros((epochs, p))
	on_vector = [True] * p

	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())

		feed_dict_train = {x: X_train}
		for j in range(p):
			feed_dict_train[y_list[j]] = np.reshape(Y_train[:, j], newshape = (N_train, 1))

		feed_dict_test = {x: X_test}
		for j in range(p):
			feed_dict_test[y_list[j]] = np.reshape(Y_test[:, j], newshape = (N_test, 1))

		# Counter for saving results after loss_step epochs
		counter = 0

		if batched:
			for epoch in range(epochs):

				for i in range(n_batches - 1):
					x_batch = X_train[range(i * mbsize, (i + 1) * mbsize), :]
					y_batch = Y_train[range(i * mbsize, (i + 1) * mbsize), :]

					feed_dict_batch = {x: x_batch}
					for j in range(p):
						feed_dict_batch[y_list[j]] = np.reshape(y_batch[:, j], newshape = (mbsize, 1))

					optimizer.run_optimizer(sess, feed_dict = feed_dict_batch)

				# Final batch (with potentially irregular size)
				x_batch = X_train[range(i * mbsize, N_train), :]
				y_batch = Y_train[range(i * mbsize, N_train), :]

				feed_dict_batch = {x: x_batch}
				for j in range(p):
					feed_dict_batch[y_list[j]] = np.reshape(y_batch[:, j], newshape = (N_train - i * mbsize, 1))

				optimizer.run_optimizer(sess, feed_dict = feed_dict_batch)

				if epoch % loss_step == 0:
					# Save results of this epoch
					train_loss[counter, :] = optimizer.compute_loss(sess, feed_dict = feed_dict_train)
					test_loss[counter, :] = optimizer.compute_loss(sess, feed_dict = feed_dict_test)
					
					# Print progress
					if verbose:
						print('----------')
						print('epoch %d' % epoch)
						print('train error: %f' % train_loss[counter, 0])
						print('test error: %f' % test_loss[counter, 0])
						print('----------')

					# Check if optimization should stop
					# if counter > 0:
					# 	changed = False
					# 	for target in range(p):
					# 		if test_loss[counter, target] > test_loss[counter - 1, target]:
					# 			on_vector[target] = False
					# 			changed = True
					# 	if changed:
					# 		# Can turn series off only if using momentum optimizer...
					# 		if sum(on_vector) > 0 and opt == 'momentum':
					# 			optimizer.deactivate_series(sess, on_vector)
					# 			var_list = [var for var in tf.global_variables() if 'Momentum' in var.name]
					# 			sess.run(tf.variables_initializer(var_list))
					# 		else:
					# 			break
					# counter += 1


		else:
			for epoch in range(epochs):

				optimizer.run_optimizer(sess, feed_dict = feed_dict_train)

				if epoch % loss_step == 0:
					# Save results of this epoch
					train_loss[counter, :] = optimizer.compute_loss(sess, feed_dict = feed_dict_train)
					test_loss[counter, :] = optimizer.compute_loss(sess, feed_dict = feed_dict_test)
					
					# Print progress
					if verbose:
						print('----------')
						print('epoch %d' % epoch)
						print('train error: %f' % train_loss[counter, 0])
						print('test error: %f' % test_loss[counter, 0])
						print('----------')

					# Check if optimization should stop
					# if counter > 0:
					# 	changed = False
					# 	for target in range(p):
					# 		if test_loss[counter, target] > test_loss[counter - 1, target]:
					# 			on_vector[target] = False
					# 			changed = True
					# 	if changed:
					# 		# Can turn series off only if using momentum optimizer...
					# 		if sum(on_vector) > 0 and opt == 'momentum':
					# 			optimizer.deactivate_series(sess, on_vector)
					# 		else:
					# 			break
					# counter += 1


		# Save final values for each network's initial layer
		W_final = []
		for j in range(p):
			W_final.append(sess.run(W_list[j]))

		if verbose:
			print('Done training')

	return test_loss[range(counter), :], train_loss[range(counter), :], W_final
