from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import glob
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import six
import torch

from toolz.itertoolz import interleave

from metrics import tpr_fpr, calculate_AUC


parser = argparse.ArgumentParser()
parser.add_argument('resdir', type=str, default=None,
                    help='directory containing results to process')
parser.add_argument('--thresh', type=float, default=0.001,
                    help='threshold for tpr/fpr computation')
parser.add_argument('--ignore-invalid', type=bool,
                    help='ignore series that did not return valid weights')
args = parser.parse_args()

resdir = args.resdir
if resdir is None:
    raise ValueError("must specify directory where results are located")

thresh = args.thresh
if thresh <= 0.:
    raise ValueError("threshold must be > 0.")

ignore_invalid = args.ignore_invalid

res_files = glob.glob(os.path.join(resdir, "*.pth.tar"))
res_files = [os.path.basename(f) for f in res_files]

if len(res_files) < 1:
    raise ValueError("No result files found in %s" % resdir)


def rectify_roc(fpr_vals, tpr_vals):
    fpr_inds = np.argsort(fpr_vals)
    fpr_sort = fpr_vals[fpr_inds]
    tpr_sort = tpr_vals[fpr_inds]
    
    fpr_cvx = [fpr_sort[0]]
    tpr_cvx = [tpr_sort[0]]
    i = 0
    while i < len(fpr_sort):
        dy = tpr_sort[i+1:] - tpr_cvx[-1]
        next_idx = np.where(dy > 0)[0]
        if len(next_idx) > 0:
            next_idx = next_idx[0] + 1
            i += next_idx
            fpr_cvx.append(fpr_sort[i])
            tpr_cvx.append(tpr_sort[i])
        else:
            break

    # if the final point isn't (1,1), then append it (for plotting)
    if not (np.allclose(fpr_cvx[-1], 1.) and np.allclose(tpr_cvx[-1], 1.)):
        fpr_cvx.append(1.)
        tpr_cvx.append(1.)
    # same for left point being (0,0)
    if not (np.allclose(fpr_cvx[0], 0.) and np.allclose(tpr_cvx[0], 0.)):
        fpr_cvx.insert(0, 0.)
        tpr_cvx.insert(0, 0.)

    return np.array(fpr_cvx), np.array(tpr_cvx)

#figdir = os.path.join(resdir, 'figs')
#if not os.path.exists(figdir):
#    os.makedirs(figdir)


# for each result file grab the parameters and stick them in a pandas dataframe
all_par_dict_list = list()
for rf in res_files:
    res = torch.load(os.path.join(resdir, rf))
    
    exper_par = res['exper_par']
    exper_keys = list(six.iterkeys(exper_par))
    alg_par = res['alg_par']
    alg_keys = list(six.iterkeys(alg_par))

    # combine the dicts
    exper_par.update(alg_par)

    exper_par['filename'] = rf
    
    all_par_dict_list.append(exper_par)

exper_df = pd.DataFrame(all_par_dict_list)

lambda_vals = np.sort(exper_df['lam'].unique())

exper_keys = ['T', 'p', 'lag', 'sparsity', 'penalty_type', 'nepoch', 'mbsize']
df_exper_par = exper_df.groupby(exper_keys)

for e_name, e_group in df_exper_par:

    series_df = e_group.groupby('pind')

    for p_grp_name, p_grp in series_df:

        p_ind = p_grp['pind'].values[0]
        df_seed = p_grp.groupby('seed')
        tpr_vals_restarts = list()
        fpr_vals_restarts = list()
        auc_vals_restarts = list()

        for seed_grp_name, seed_grp in df_seed:

            seed = seed_grp['seed'].values[0]

            tpr_vals = list()
            fpr_vals = list()

            for li, lam_val in enumerate(lambda_vals):

                df_lambda = seed_grp.loc[seed_grp['lam'] == lam_val]

                filenames = df_lambda['filename'].values
                assert len(filenames) == 1
                fn = filenames[0]

                p = np.unique(df_lambda['p'].values)[0]

                GC_true = np.zeros(p, dtype=np.int)
                GC_est = np.zeros(p, dtype=np.int)

                res = torch.load(os.path.join(resdir, fn))
                weights = res['weights']
                exper_pars = res['exper_par']
                alg_pars = res['alg_par']
                p_ind = exper_pars['pind']

                nlags = alg_pars['lag_train'] 

                if weights is not None:
                   for i in range(p):
                       gamma = np.linalg.norm(weights[:,i*nlags:(i+1)*nlags])
                       GC_est[i] = (gamma > thresh).astype(np.int)
                   GC_true[:] = res['GC']
                else:
                    print(fn)
                    print("no valid results for series %d seed %d, skipping" % (p_ind, seed_grp['seed']))
                    continue

                tpr, fpr = tpr_fpr(GC_est, GC_true, thresh)
                tpr_vals.append(tpr)
                fpr_vals.append(fpr)

            lambda_vals = np.array(lambda_vals)
            lam_inds = np.argsort(lambda_vals)[::-1]
            tpr_vals = np.array(tpr_vals)
            tpr_vals = tpr_vals[lam_inds]
            fpr_vals = np.array(fpr_vals)
            fpr_vals = fpr_vals[lam_inds]

            fpr_vals, tpr_vals = rectify_roc(fpr_vals, tpr_vals)

            # compute ROC and AUC using TPR/FPR vals
            auc = calculate_AUC(tpr_vals[np.isfinite(tpr_vals)],
                                fpr_vals[np.isfinite(fpr_vals)])

            tpr_vals_restarts.append(tpr_vals)
            fpr_vals_restarts.append(fpr_vals)
            auc_vals_restarts.append(auc)

        # plot avg. ROC curve
        roc_fig, roc_axes = plt.subplots(1,2)
        #tpr_vals_restarts = np.vstack(tpr_vals_restarts)
        #fpr_vals_restarts = np.vstack(fpr_vals_restarts)
        #auc_vals_restarts = np.vstack(auc_vals_restarts)
        #tpr_mean = np.nanmean(tpr_vals_restarts, axis=0)
        #tpr_std = np.nanstd(tpr_vals_restarts, axis=0)
        #tpr_num = np.sum(np.isfinite(tpr_vals_restarts), axis=0)
        #fpr_mean = np.nanmean(fpr_vals_restarts, axis=0)
        #fpr_std = np.nanstd(fpr_vals_restarts, axis=0)
        #fpr_num = np.sum(np.isfinite(fpr_vals_restarts), axis=0)
        #auc_mean = np.nanmean(auc_vals_restarts)
        #auc_se = np.nanstd(auc_vals_restarts) / np.sum(np.isfinite(auc_vals_restarts))
        #roc_axes[0].plot(fpr_mean, tpr_mean)
        #roc_axes[0].fill_between(fpr_mean, tpr_mean, y2=tpr_mean+(tpr_std/np.sqrt(tpr_num)), alpha=0.25, color='steelblue')
        #roc_axes[0].fill_between(fpr_mean, tpr_mean, y2=tpr_mean-(tpr_std/np.sqrt(tpr_num)), alpha=0.25, color='steelblue')

        auc_mean = np.nanmean(np.array(auc_vals_restarts))
        auc_se = np.nanstd(np.array(auc_vals_restarts)) / np.sum(np.isfinite(auc_vals_restarts))
        for tpr, fpr in zip(tpr_vals_restarts, fpr_vals_restarts):
            roc_axes[0].plot(fpr, tpr, alpha=0.4, color='steelblue')

        roc_axes[0].plot([0., 1.], [0., 1.], alpha=0.7, color='gray')
        roc_axes[0].set_title('ROC')
        roc_axes[0].set_xlabel('FPR')
        roc_axes[0].set_ylabel('TPR')
        roc_axes[0].text(0.2, 0.0, 'AUC: %.3f +- %.4f' % (auc_mean, 1.96*auc_se))
        roc_axes[0].axis('square')
        roc_axes[1].boxplot(auc_vals_restarts)
        roc_axes[1].scatter(0.1+np.ones_like(auc_vals_restarts), auc_vals_restarts,
                            color='steelblue', marker='x')
        roc_axes[1].set_xticks([])
        roc_axes[1].axis('square')
        roc_axes[1].set_ylim(0., 1.)
        roc_axes[1].set_title('AUC distribution')
        
        roc_fig.tight_layout()
        figname = 'pind-%d_' % p_ind
        figname += '_'.join(map('-'.join, zip(exper_keys, map(str, e_name))))
        figname += ".pdf"
        roc_fig.savefig(figname, bbox_layout='tight')

        plt.close(roc_fig)


# make ROC plot for all series jointly
for e_name, e_group in df_exper_par:

    series_df = e_group.groupby('pind')

    tpr_vals_restarts = list()
    fpr_vals_restarts = list()
    auc_vals_restarts = list()

    df_seed = e_group.groupby('seed')
    for seed_grp_name, seed_grp in df_seed:

        seed = seed_grp['seed'].values[0]

        tpr_vals = list()
        fpr_vals = list()

        for li, lam_val in enumerate(lambda_vals):

            df_lambda = seed_grp.loc[seed_grp['lam'] == lam_val]

            filenames = df_lambda['filename'].values

            p = np.unique(df_lambda['p'].values)[0]

            GC_est = np.zeros((p, p), dtype=np.int)
            GC_true = np.zeros((p, p), dtype=np.int)

            for fn in filenames:

                res = torch.load(os.path.join(resdir, fn))
                weights = res['weights']
                exper_pars = res['exper_par']
                alg_pars = res['alg_par']
                p_ind = exper_pars['pind']

                nlags = alg_pars['lag_train']

                if weights is not None:
                   for i in range(p):
                       gamma = np.linalg.norm(weights[:,i*nlags:(i+1)*nlags])
                       GC_est[p_ind,i] = (gamma > thresh).astype(np.int)
                   GC_true[p_ind] = res['GC']
                else:
                    print(fn)
                    print("no valid results for series %d seed %d, skipping" % (p_ind, seed_grp['seed']))
                    continue

            tpr, fpr = tpr_fpr(GC_est, GC_true, thresh)
            tpr_vals.append(tpr)
            fpr_vals.append(fpr)

        lambda_vals = np.array(lambda_vals)
        lam_inds = np.argsort(lambda_vals)[::-1]
        tpr_vals = np.array(tpr_vals)
        tpr_vals = tpr_vals[lam_inds]
        fpr_vals = np.array(fpr_vals)
        fpr_vals = fpr_vals[lam_inds]

        fpr_vals, tpr_vals = rectify_roc(fpr_vals, tpr_vals)

        auc = calculate_AUC(tpr_vals[np.isfinite(tpr_vals)],
                            fpr_vals[np.isfinite(fpr_vals)])

        tpr_vals_restarts.append(tpr_vals)
        fpr_vals_restarts.append(fpr_vals)
        auc_vals_restarts.append(auc)

        # plot avg. ROC curve
        roc_fig, roc_axes = plt.subplots(1,2)
        #tpr_vals_restarts = np.vstack(tpr_vals_restarts)
        #fpr_vals_restarts = np.vstack(fpr_vals_restarts)
        #auc_vals_restarts = np.vstack(auc_vals_restarts)
        #tpr_mean = np.nanmean(tpr_vals_restarts, axis=0)
        #tpr_std = np.nanstd(tpr_vals_restarts, axis=0)
        #tpr_num = np.sum(np.isfinite(tpr_vals_restarts), axis=0)
        #fpr_mean = np.nanmean(fpr_vals_restarts, axis=0)
        #fpr_std = np.nanstd(fpr_vals_restarts, axis=0)
        #fpr_num = np.sum(np.isfinite(fpr_vals_restarts), axis=0)
        #auc_mean = np.nanmean(auc_vals_restarts)
        #auc_se = np.nanstd(auc_vals_restarts) / np.sum(np.isfinite(auc_vals_restarts))
        #roc_axes[0].plot(fpr_mean, tpr_mean)
        #roc_axes[0].fill_between(fpr_mean, tpr_mean, y2=tpr_mean+(tpr_std/np.sqrt(tpr_num)), alpha=0.25, color='steelblue')
        #roc_axes[0].fill_between(fpr_mean, tpr_mean, y2=tpr_mean-(tpr_std/np.sqrt(tpr_num)), alpha=0.25, color='steelblue')

        auc_mean = np.nanmean(np.array(auc_vals_restarts))
        auc_se = np.nanstd(np.array(auc_vals_restarts)) / np.sum(np.isfinite(auc_vals_restarts))
        for tpr, fpr in zip(tpr_vals_restarts, fpr_vals_restarts):
            roc_axes[0].plot(fpr, tpr, alpha=0.4, color='steelblue')

        roc_axes[0].plot([0., 1.], [0., 1.], alpha=0.7, color='gray')
        roc_axes[0].set_title('ROC')
        roc_axes[0].set_xlabel('FPR')
        roc_axes[0].set_ylabel('TPR')
        roc_axes[0].text(0.2, 0.0, 'AUC: %.3f +- %.4f' % (auc_mean, 1.96*auc_se))
        roc_axes[0].axis('square')
        roc_axes[1].boxplot(auc_vals_restarts)
        roc_axes[1].scatter(0.1+np.ones_like(auc_vals_restarts), auc_vals_restarts,
                            color='steelblue', marker='x')
        roc_axes[1].set_xticks([])
        roc_axes[1].axis('square')
        roc_axes[1].set_ylim(0., 1.)
        roc_axes[1].set_title('AUC distribution')
        
        roc_fig.tight_layout()
        figname = '_'.join(map('-'.join, zip(exper_keys, map(str, e_name))))
        figname += ".pdf"
        roc_fig.savefig(figname, bbox_layout='tight')

        plt.close(roc_fig)
