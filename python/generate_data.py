from __future__ import division
import numpy as np
import scipy.misc
import math



"""""
Function generates data from a lorenz-96 model. We use the 
Euler method and add a small amount of noise between time steps
to better simulate a nonlinear time series model. The stability of
the simulation is sensitive to the parameters. I've found that
setting the integration step size (delta_t) to greater than about .03
leads to bad stability. Also, the standard deviation of the noise (sd)
added at each time step also affects things. Anything greater than .1 
seems to lead to instability. I suspect there are trade offs in instability
also between the sd and delta_t but I don't quite know what they are.

inputs:
forcing_constant - default set to 5, but can play around with this.
p - dimensionality of system
N - length of series
delta_t - the step size in Eurler's method
sd - the standard deviation of the noise
noise_add - if "step" noise is added at each time step to the variables
            if "global" noise is added at the end after the entire simulation.

"""""
def lorenz_96(forcing_constant,p,N,delta_t = .01,sd = .1,noise_add = "global"):
    burnin = 4000
    N += burnin
    z = np.zeros((N,p))
    z[0,:] = .01*np.random.randn(p)
    for t in range(1,N):
        for i in range(p):
            upone = i + 1
            if i == (p - 1):
                upone = 0
            grad = (z[t-1,upone] - z[t-1,i-2])*z[t-1,i - 1] - z[t-1,i] + forcing_constant
            z[t,i] = delta_t*grad + z[t - 1,i]
            if noise_add == "step":
                z[t,i] += sd*np.random.randn(1)

    if noise_add == "global":
        z += sd*np.random.randn(N,p)

    return(z[range(burnin,N),:])

def linear_model(sparsity,p,sd_beta,sd_x,sd_e,N):
    #s = np.random.binomial(p,sparsity,size=None)
    n_on = np.floor(p*sparsity)
    s = np.concatenate((np.zeros(p - n_on),np.ones(n_on)),0)
    beta = np.random.normal(0,sd_beta,p)
    beta_new = np.multiply(s,beta)

    X = sd_x*np.random.randn(N,p)
    Y = np.dot(X,beta_new) + np.random.normal(0,sd_e)
    return(Y,X,s)

#def NN_model(sprasity,p,sd_W,sd_x,sd_e,N):

#def non_linear_model(sparsity,p,sd_x,sd_e,N):
#    n_on = np.floor(p*sparsity)
#    s = np.concatenate((np.zeros(p - n_on),np.ones(n_on)),0)
  

def stationary_var(beta,p,lag,radius):
    not_stationary = True


    bottom = np.hstack((np.eye(p*(lag-1)),np.zeros((p*(lag - 1),p))))  
    beta_tilde = np.vstack((beta,bottom))
    eig = np.linalg.eigvals(beta_tilde)
    maxeig = max(np.absolute(eig))
    if (maxeig < radius):
        not_stationary = False
    return(beta*.95,not_stationary)



""" Generate linear VAR dynamics.
        Intput:
        sparsity: sparsity fraction of the Granger causality graph
        p: dimension of VAR model
        sd_beta: standard deviation of the random nonzero entries
        in transition matrices
        sd_e: standard deviation of the independent errors in the VAR model
        N: length of generate time series
        lag: the lag order of the VAR model

        Output:
        X: p x N matrix contain the random time series
        beta: the transition matrices of the VAR model
        GC_on: p x p granger causality graph of the time series 
        (entry ij means series j does not Granger cause series i)
    """
def linear_ts_model(sparsity,p,sd_beta,sd_e,N,lag):
    radius = .97
    min_effect = 1
    n_on = int(np.floor(p*sparsity))
    s = np.concatenate((np.zeros(p - n_on),np.ones(n_on)),0)
    beta = sd_beta*np.random.randn(p,p*lag)
    beta[(beta < min_effect) & (beta > 0)] = min_effect
    beta[(beta > - min_effect) & (beta < 0)] = -min_effect

    GC_on = np.random.binomial(1,sparsity,p*p).reshape(p,p)
    for i in range(0,p):
        print(i)
        beta[i,i] = min_effect
        GC_on[i,i] = 1

    GC_lag = GC_on
    for i in range(0,lag - 1):
        GC_lag = np.hstack((GC_lag,GC_on))

    print(GC_lag)
    print(beta)

    beta = np.multiply(GC_lag,beta)
    errors = sd_e*np.random.randn(p,N)

    not_stationary = True
    while (not_stationary):
        beta,not_stationary = stationary_var(beta,p,lag,radius)



    X = np.zeros((p,N))
    X[:,range(0,lag)] = errors[:,range(0,lag)]
    for i in range(lag,N):
        X[:,i] = np.dot(beta,X[:,range(i-lag,i)].flatten(order='F')) + errors[:,i]

    return(X,beta,GC_on)


def gen_nonlinear_three_node(T, mode='fanout'):
    """ Generate three node nonlinear dynamics.
        
        From "Supplementary information for detecting causality from nonlinear
        dynamcis with short-term time series"

        T : int
        length of generated series

        mode : str
        'fanout' or 'fanin'

        Returns:

        Y : 3 x T ndarray
        generated time series
    """
    Ttrans = 100
    Y = np.zeros([3, T+Ttrans])
    
    if mode == 'fanout':
        gamma = np.array([[4., 0., 0.,], [0.21, 3.1, 0.], [-0.636, 0., 2.12]])
    elif mode == 'fanin':
        gamma = np.array([[4., 0., 0.,], [0., 3.6, 0.], [0.636, -0.636, 2.12]])

    Y[:,0] = np.random.rand(3)

    gamma_d = np.diag(gamma)

    for t in range(1,T+Ttrans):
        Y[:,t] = Y[:,t-1]*(gamma_d - np.dot(gamma, Y[:,t-1]))
        #for j in range(3):
        #    Y[j,t] = Y[j,t-1]*(gamma_d[j] - np.dot(gamma[j,:], Y[:,t-1]))
    
    return np.require(Y[:,:T], requirements='C')
 


def gen_chaotic_lotka_volterra(T):
    """ Generate three node chaotic Lotka-Volterra dynamics.
        
        From "Supplementary information for detecting causality from nonlinear
        dynamcis with short-term time series"

        T : int
        length of generated series

        Returns:

        Y : 3 x T ndarray
        generated time series
    """
    Ttrans = 100
    X = np.zeros([3, T+Ttrans])

    X[:,0] = np.random.rand(3)

    for t in xrange(1,T+Ttrans):
        X[0,t] = X[0,t-1]*(2. - X[0,t-1] + 0.675*X[1,t-1] - 0.5*X[2,t-1])
        X[1,t] = X[1,t-1]*(2. - 0.5*X[0,t-1] - X[1,t-1] + 0.675*X[2,t-1])
        X[2,t] = X[2,t-1]*(2. + 0.675*X[0,t-1] - 0.5*X[1,t-1] - X[2,t-1])

    return np.require(X[:,:T], requirements='C')


#generate data of different types
def generate_data_general(dtype,sparsity,p,sd_beta,sd_e,N,lag):
    if (dtype == "VAR"):
        Y,beta,GC = linear_ts_model(sparsity,p,sd_beta,sd_e,N,lag)
        return(Y,beta,GC)

"""returns the TS data set in input output format for a particular dimension
    as output. The order in the input is such that the lags for a particular ts
    are continguous.

    Y is the input. This is of dimension p (number of series) and N (number of time points). 
    tr_ts_prop is the proprtion of the data set to split into training and testing
    p_i is the index of the time series used as output.

    The output of the function is four variables: X_train is a p*lag x Ntrain matrix.
    Y_train is the values of the output series (p_i) and is of dimension 1 x Ntrain.
    Y_test and X_test are the same but for the testing data. """
def convert_ts_data_to_standard_training_format(Y,p_i,lag,tr_ts_prop=.1):
    p,N = Y.shape
    N_new = N - lag
    X_out = np.zeros((p*lag,N_new))
    Y_out = np.zeros(N_new)
    for i in range(lag,N):
        X_out[:,i-lag] = np.fliplr(Y[:,range(i - lag,i)]).flatten(order='C')
        Y_out[i-lag] = Y[p_i,i]


    Ntrain = int(math.floor((N_new)*(1 - tr_ts_prop)))
    Ntest = N_new - Ntrain

    X_train = X_out[:,range(Ntrain)]
    Y_train = Y_out[range(Ntrain)]
    X_test = X_out[:,range(Ntrain,N_new)]
    Y_test = Y_out[range(Ntrain,N_new)]

    return(X_train,Y_train,X_test,Y_test)

"""function generates multivariate time series data from a 
sparse HMM model where there is sparsity in the HMM transition function
inputs:
p - dimensional of the time series
N - length of time series
num_states - #of states of the HMM
sd_e - standard deviation of the error distribution for HMM
sparsity - sparsity proportion
tau - temperature parameter
you might need to mess with the sd_e parameter, as a bigger value will 
make dependence on the past more important for inference of the future.
"""
def generate_data_hmm(p,N,num_states = 3,sd_e = .1,sparsity=.2,tau=2):
    Z_sig = .3
    Z = np.zeros((p,p,num_states,num_states))
    GC_on = np.random.binomial(1,sparsity,p*p).reshape(p,p)
    for i in range(p):
        GC_on[i,i] = 1
    mu = np.random.randn(p,num_states);
    for i in range(p):
        for j in range(p):
            if GC_on[i,j]:
                Z[i,j,:,:] = Z_sig*np.random.randn(num_states,num_states)

    ##generate state sequence
    L = np.zeros((N,p))
    for t in range(1,N):
        for i in range(p):
            switch_prob = np.zeros(num_states)
            for j in range(p):
                switch_prob += Z[i,j,L[t-1,j],:]
            switch_prob = switch_prob*tau
            switch_prob = np.exp(switch_prob - scipy.misc.logsumexp(switch_prob))
            print(switch_prob)
            L[t,i] = np.nonzero(np.random.multinomial(1,switch_prob))[0][0]


    ##generate outputs from state sequence
    X = np.zeros((N,p))
    for i in range(N):
        for j in range(p):
            X[i,j] = sd_e*np.random.randn(1) + mu[j,L[i,j]]

    return(X,L,GC_on)





if __name__ == "__main__": 
    # p = 20
    # N = 100
    # sparsity = .2
    # sd_beta = 1
    # sd_x = 1
    # sd_e = .1
    # lag = 3
    # #Y,X,s = linear_model(sparsity,p,sd_beta,sd_x,sd_e,N)
    # Y,beta,GC = linear_ts_model(sparsity,p,sd_beta,sd_e,N,lag)


    # ###testing false positive code
    # Gamma = np.zeros((3,3))
    # GC = np.zeros((3,3))
    # Gamma[1,1] = 2
    # GC[1,1] = 1
    # Gamma[0,0] = .2
    # GC[2,2] = 1
    # tp,fp = calculate_false_positive_negative(Gamma,GC,.1)
    # print(tp)
    # print(fp)


    # ###testing AUC code
    # fp = np.array([.01,.1,.4,.8,.9])
    # tp = np.array([.1,.3,.8,.85,.95])
    # auc = calculate_AUC(tp,fp)
    # print(auc)

    X,L,GC = generate_data_hmm(5,100)


    #np.random.seed(121)
    #forcing_constant = 10
    #p = 10
    #N = 1000
    #z = lorenz_96(forcing_constant,p,N)

    #import matplotlib.pyplot as plt
    #plt.plot(z[range(1000),7])
    #plt.show()





