from __future__ import division
from __future__ import print_function

import numpy as np
import time

from itertools import product

dstamp = time.strftime("%Y%m%d")
tstamp = time.strftime("%H%M%S")

T_grid = [1e4]  #[1e2, 1e3, 1e4]
p_grid = [20]
lag_grid = [1]
spars_grid = [0.3]

# lag_train is derived from penalty type
lam_grid = np.logspace(-5., -2., num=100)
lr_grid = 10.**np.arange(-8, -1)
nepochs_grid = [10000]
pen_types = ['h_truncation']  #['group_lasso', 'h_truncation']
opt_types = ['proximal']

baseseed = 8675309
num_random = 20
nrestarts = 10

param_grid = product(T_grid, p_grid, lag_grid, spars_grid, lam_grid,
                     nepochs_grid, pen_types, opt_types)

jobname = "exp_mlp_var_%s_%s" % (dstamp, tstamp)
jobfile = "%s.job" % jobname

BASECMD = "python exp_mlp_var.py"

OUTDIR = "./out/%s" % jobname
CHKDIR = "./checkpoints/%s" % jobname

with open(jobfile, 'w') as f:

    for param in param_grid:
        T, p, lag, sparsity, lam, nepochs, pen_type, opt_type = param
    
        if pen_type == 'group_lasso':
            lag_train = lag
        elif pen_type == 'h_truncation':
            lag_train = 3*lag

        for si in range(num_random):
            for pi in range(p):
                argstr = " --T=%d --p=%d --lag=%d --sparsity=%e "
                argstr += " --lag_train=%d --pind=%d --lam=%e --nepoch=%d"
                argstr += " --penalty_type=%s --opt_type=%s --seed=%d"
                argstr += " --nrestarts=%d" % nrestarts
                argstr += " --outdir=%s --chkpointdir=%s --verbose=1 "
                argstr += " ".join(['--lr=%e' % lr for lr in lr_grid])
                argstr = argstr % (T, p, lag, sparsity, lag_train, pi, lam,
                                   nepochs, pen_type, opt_type, baseseed+si, OUTDIR, CHKDIR)
                f.write(BASECMD + argstr + '\n')
