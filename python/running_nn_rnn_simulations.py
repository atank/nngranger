from __future__ import division
from generate_data import generate_data_general, convert_ts_data_to_standard_training_format
from rnn_training_funcs import optimize_rnn
import matplotlib.pyplot as plt
import numpy as np

data_type = "VAR"
penalty_type = "group_lasso"
lag = 1
p = 6
mbatch_size = 10
lr = .01
lam = .01
N = 1000
sparsity = .3
sd_beta = 1
sd_x = 1
sd_e = .1
lag = 1
np.random.seed(12321)

###generate simulation data for NN model
Y,beta,GC = generate_data_general(data_type,sparsity,p,sd_beta,sd_e,N,lag)
N_test = int(N/4); N_train = N - N_test
Y_train = Y[:,range(N_train)]; Y_test = Y[:,range(N_train,N)]

###convert TS data into standard input-output format
p_i = 2##GC index
#X,Y,Xtest,Ytest = convert_ts_data_to_standard_training_format(Y,p_i,lag)
params,test_loss,train_loss = optimize_rnn(Y_train,Y_test,p,p_i,penalty_type,lam,opt_type="proximal")

#test_prob,train_prob_list,params = cross_validation_nn(X,Y,Xtest,Ytest,lag,p,penalty_type,lam_list,lr_list,nepoch=500000,opt_type="proximal")
#compute best test error from a linear model, to see how well the lstm is doing.


gamma = np.linalg.norm(params, axis=0)
print((gamma > 0)*1)
print(GC[p_i,:])

plt.plot(train_loss)
plt.ylabel('training loss')
plt.show()