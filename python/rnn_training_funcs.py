from __future__ import division
from nn_autoreg_pytorch import LSTM_realout
import torch.nn as nn
import torch
import random
import numpy
from torch.autograd import Variable
import torch.optim as optim
import itertools


def proximal_update_rnn(W,p,lam,update_type = "group_lasso"):
    #W_temp = W.numpy().copy()
    W_temp = W.copy()
    #print(W_temp.shape)
    if update_type == "group_lasso":
        for i in range(p):
            W_temp[:,i] = proximal_group_update(W_temp[:,i],lam)
                #for input to hidden interaction

    return(W - W_temp)


def proximal_group_update(v,lam):
    vc = v.copy()
    norm_value = numpy.linalg.norm(vc)
    if (lam == 0):
        return(vc)
    if (norm_value < lam):
        vc = 0
    else:
        vc = vc*(1 - lam/norm_value)

    return(vc)


def repackage_hidden(h):
    """Wraps hidden states in new Variables, to detach them from their history."""
    if type(h) == Variable:
        return Variable(h.data)
    else:
        return tuple(repackage_hidden(v) for v in h)

def torch_group_loss(net,p,n_hidden):
    f_layer = list(net.parameters())[0]
    ff = []
    for i in range(p):
        ff.append(torch.norm(f_layer[:,i],2))
            
    outloss = sum(ff)
    return(outloss)

def compute_loss_rnn(X,net,criterion,lam,update_type,p,n_hidden,q):
    Ntrain,p = X.shape
    input_batch = Variable(torch.from_numpy(X).float())
    loss = rnn_recursion_loss(Ntrain,0,criterion,input_batch,q,net)
    loss = sum(loss,compute_torch_penalty_loss(net,update_type,p,lam,n_hidden))
    return(loss)

def rnn_recursion_loss(end_ind,start_ind,criterion,input_batch,q,net):
    net.init_hidden()
    loss_list = []
    hidden = net.init_hidden()
    for i in range(end_ind-start_ind-1):
        input = input_batch[i,:]
        output,hidden = net(input.view(1,1,-1),hidden)
        target = input_batch[i+1,q]
        loss_list.append(criterion(output,target))
        hidden = repackage_hidden(hidden)
    loss = sum(loss_list)
    return(loss)


def compute_torch_penalty_loss(net,update_type,p,lam,n_hidden):
    if (update_type == "group_lasso"):
        penalty_loss = torch_group_loss(net,p,n_hidden)
    return(lam*penalty_loss)


def optimize_rnn(Xtrain,Xtest,p,q,update_type,lam,nepoch=300,mbatch=2,lr=.01,prox_batch=1,loss_step=2,n_hidden = 3,opt_type="subgradient"):
    p,Ntrain = Xtrain.shape
    p,Ntest = Xtest.shape
    mbatch = Ntrain

    net = LSTM_realout(p,n_hidden*p)
    criterion = nn.MSELoss()
    Nbatches = int(Ntrain/mbatch)
    iteration = 0
    batch_list = range(1,Nbatches+1)
    train_loss = []
    test_loss = []
    #if (opt_type == "subgradient"):
    optimizer = optim.SGD(net.parameters(), lr=0.001,momentum=.9)


    param = list(net.parameters())
    print(param[0].data)
    for epoch in range(nepoch):
        dat_order = random.sample(batch_list,len(batch_list))
        iteration += 1
        if (epoch % loss_step == 0):
            print(epoch)
            train_loss = numpy.append(train_loss,compute_loss_rnn(numpy.transpose(Xtrain),net,criterion,lam,update_type,p,n_hidden,q).data.numpy())
            #test_loss = numpy.append(test_loss,compute_loss_rnn(numpy.transpose(Xtest),net,criterion,0,update_type,p,n_hidden,q)) 
        for j in range(len(dat_order)):
            #print()
            batchnum = dat_order[j]
            start_ind = (batchnum - 1)*mbatch
            end_ind = batchnum*mbatch
            inp_torch = torch.from_numpy(numpy.transpose(Xtrain[:,start_ind:end_ind]))
            input_batch = Variable(inp_torch.float())
            loss = rnn_recursion_loss(end_ind,start_ind,criterion,input_batch,q,net)

            if (opt_type == "subgradient"):
                loss = sum(loss,compute_torch_penalty_loss(net,update_type,p,lam,n_hidden))
            net.zero_grad() 
            loss.backward()

            #if (opt_type == "subgradient"):
            optimizer.step()

            param = list(net.parameters())
            #print(param[0].grad.data)
            if (opt_type == "proximal"):
                param = list(net.parameters())
                if ((iteration % prox_batch) == 0):
                    update_weight = proximal_update_rnn(param[0].data.numpy(),p,lam,update_type)
                    param[0].data.sub_(torch.from_numpy(update_weight).float())



    print('Finished Training')

    ##compute test error###

    param_0 = list(net.parameters())[0].data.numpy()

    return(param_0,test_loss,train_loss)