import tensorflow as tf
from li_regularizer import *
from generate_data import *
import numpy as np
import sys
from scipy.io import savemat
import matplotlib.pyplot as plt
from sklearn.metrics import auc,precision_recall_curve
plt.ion()


p = 20
N_train = 2000
N_test = 1000
sparsity = .5
sd_beta = 40
sd_x = 1
sd_e = .1
Yo,Xo,s = linear_model(sparsity,p,sd_beta,sd_x,sd_e,N_train + N_test)
keep_prob = .999

X = Xo[range(0,N_train),]
Y = Yo[range(0,N_train),]
X_test = Xo[range(N_train,N_train + N_test),]
Y_test = Yo[range(N_train,N_train + N_test),]



#lambda_l1 = float(sys.argv[1])
#lambda_l2 = float(sys.argv[2])
#lambda_dropinput = float(sys.argv[1])
lambda_dropinput = 1


#keep_prob = float(sys.argv[4])  # keep_prob \in (0, 1]
#threshold = float(sys.argv[2])
threshold = .0001

learning_rate_ini = 0.001
training_epochs = 1000
batch_size = 200
display_step = 1
examples_to_show = 10
total_batch = int(np.floor(N_train/batch_size))

# Network Parameters
n_hidden_1 = 20  # 1st layer num features
#n_hidden_2 = 10  # 2nd layer num features

# tf Graph input (only pictures)
x = tf.placeholder("float", [None, p])
y_ = tf.placeholder("float",[None,1])

W = {
    'h1': tf.Variable(tf.random_normal([p, n_hidden_1], stddev=1)),
    #'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2], stddev=1)),
    'h3': tf.Variable(tf.random_normal([n_hidden_1, 1], stddev=1))
}

W_prune = W

biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1], stddev=1)),
    #'b2': tf.Variable(tf.random_normal([n_hidden_2], stddev=1)),
    'b3': tf.Variable(tf.random_normal([1], stddev=1)),
}

# Building the encoder
def feed_forward(x):
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, W['h1']), biases['b1']))
    tf.nn.dropout(layer_1, keep_prob)
    #layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, W['h2']), biases['b2']))
    #tf.nn.dropout(layer_2, keep_prob)
    return layer_1

def dropfeature(x):
    #regularizers = tf.reduce_mean(li_regularizer(.1)(W['h1']))
    regularizers = (lo_regularizer(.99999)(W['h1']))
    regularizers = x * regularizers
    return regularizers

drop_feature_vec = np.arange(-50,10,.25)
nlam = np.shape(drop_feature_vec)[0]
auc_f = np.zeros(nlam)
final_test_loss = np.zeros(nlam)
iter_ = 0
for dropfeature_log in drop_feature_vec:
    print(iter_)
    lambda_dropinput = np.exp(dropfeature_log)

    # Construct model
    feed_forward_out = feed_forward(x)

    y_pred = tf.add(tf.matmul(feed_forward_out, W['h3']), biases['b3'])
    # Define loss and optimizer, minimize the squared error
    loss = tf.reduce_mean(tf.pow(y_- y_pred, 2))
    cost = loss
    #cost += l1(lambda_l1)
    #cost += l2(lambda_l2)
    cost += dropfeature(lambda_dropinput)

    optimizer = tf.train.AdamOptimizer(learning_rate_ini, beta1=0.9, beta2=0.999, epsilon=1e-08, use_locking=False).minimize(cost)

    # Initializing the variables
    init = tf.initialize_all_variables()


    # Launch the graph
    loss_value_train = np.zeros(training_epochs)
    loss_value_test = np.zeros(training_epochs)

    with tf.Session() as sess:
        sess.run(init)
        avg_cost = 0.
        # Training cycle
        for epoch in range(0,training_epochs):
            # Loop over all batches
            for i in range(0,total_batch):
                batch_xs = X[range((i-1)*batch_size,i*batch_size),]
                batch_ys = (Y[range((i-1)*batch_size,i*batch_size)][np.newaxis]).T
                #print(batch_ys)
                # Fit training using batch data
                optimizer.run(feed_dict={x: batch_xs, y_: batch_ys})
            loss_value_train[epoch] = sess.run(cost,feed_dict={x:X, y_:Y[np.newaxis].T})
            loss_value_test[epoch] = sess.run(loss,feed_dict={x:X_test,y_:Y_test[np.newaxis].T})
        final_test_loss[iter_] = loss_value_test[training_epochs - 1]
        print("Optimization Finished!")
        w_input = sess.run(W['h1'])
        w_inclusion = np.linalg.norm(w_input,axis=1)
        precision, recall,thresholds = precision_recall_curve(s,w_inclusion)
        auc_f[iter_] = auc(recall,precision)
        iter_ = iter_ + 1

plt.plot(x=)

#print(w_input)
#w_inclusion = np.linalg.norm(w_input,axis=0)
#precision, recall,thresholds = sklearn.metrics.precision_recall_curve(s,w_inclusion)
#auc_f = sklearn.metrics.auc(recall,precision)



