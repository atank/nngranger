from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import numpy as np
import os
import random
import torch
import torch.nn as nn
import torch.optim as optim

from torch.autograd import Variable


def proximal_update(W, lag, p, lam, update_type="group_lasso"):
    #W_temp = W.np().copy()
    W_temp = W.copy()
    #print(W_temp.shape)
    if update_type == "group_lasso":
        for i in range(p):
            ind_start = (i)*lag
            ind_end = (i + 1)*lag
            W_temp[:,ind_start:ind_end] = \
                proximal_group_update(W_temp[:,ind_start:ind_end], lam)
    if update_type == "h_truncation":
        for i in range(p):
            ind_start = (i)*lag
            ind_end = (i + 1)*lag
            for j in range(1,lag+1):
                W_temp[:,(ind_end - j):ind_end] = \
                    proximal_group_update(W_temp[:,(ind_end - j):ind_end], lam)

    return W - W_temp


def proximal_group_update(v, lam):
    vc = v.copy()
    norm_value = np.linalg.norm(v,ord='fro')
    if (lam == 0):
        return(vc)
    if (norm_value < lam):
        vc = 0
    else:
        vc = vc*(1 - lam/norm_value)

    return vc


def compute_loss(X, Y, net, criterion, lam, update_type, p, lag):
    inp_torch = torch.from_numpy(np.transpose(X))
    inp = Variable(inp_torch.float())
    output = net(inp)
    target = Variable(torch.from_numpy(Y).float())
    loss = criterion(output, target)
    penalty_loss = compute_torch_penalty_loss(net, update_type, p, lag, lam)

    total_loss = loss.data.numpy()[0] + penalty_loss.data.numpy()[0]

    return total_loss


def compute_torch_penalty_loss(net, update_type, p, lag, lam):
    if (update_type == "group_lasso"):
        penalty_loss = torch_group_loss(net,p,lag)
    if (update_type == "h_truncation"):
        penalty_loss = torch_heirarchical_loss(net,p,lag)

    return lam*penalty_loss


def group_norm(net):
    f_layer = list(net.parameters())[0].data.np()
    return np.sum(np.linalg.norm(f_layer, axis=0))


#def heirarchical_norm(net,p,lag):
#    f_layer = list(net.parameters())[0].data.np()


def torch_heirarchical_loss(net, p, lag):
    f_layer = list(net.parameters())[0]
    loss_list = []
    for i in range(0,p):
        ind_start = (i)*lag
        ind_end = (i + 1)*lag
        for j in range(1,lag + 1):
            loss_list.append(torch.norm(f_layer[:,(ind_end - j):ind_end],2))
    outloss = sum(loss_list)

    return outloss


def torch_group_loss(net, p, lag):
    #ff = []
    f_layer = list(net.parameters())[0]
    i = 0
    ind_start = (i)*lag
    ind_end = (i + 1)*lag
    ff = [torch.norm(f_layer[:,ind_start:ind_end],2)]
    #ff.append(Variable(torch.randn(1)))
    #print("here")
    #print(ff)
    #ff = ff.append(outloss)
    for i in range(1,p):
        ind_start = (i)*lag
        ind_end = (i + 1)*lag
        outloss = torch.norm(f_layer[:,ind_start:ind_end],2)
        ff.append(torch.norm(f_layer[:,ind_start:ind_end],2))
    #print(ff)
    outloss = sum(ff)
    return outloss


def optimize_nn(X, Y, Xtest, Ytest, lag, p, update_type, lam, dims=None,
                nepoch=5000, mbatch=None, lr=.01, prox_batch=1, loss_step=100,
                opt_type="subgradient", verbose=1):

    plag, Ntrain = X.shape
    plag, Ntest = Xtest.shape
    if mbatch is None:
        mbatch = Ntrain

    if dims is None:
        dims = [plag, plag, 1]

    dimpairs = list(zip(dims[:-1], dims[1:]))
    net = nn.Sequential()
    for i, (din, dout) in enumerate(dimpairs[:-1]):
        net.add_module('fc%d' % i, nn.Linear(din, dout, True))
        net.add_module('relu%d' % i, nn.ReLU())
    din, dout = dimpairs[-1]
    net.add_module('out', nn.Linear(din, dout, True))

    criterion = nn.MSELoss()
    Nbatches = int(Ntrain/mbatch)
    iteration = 0
    batch_list = range(1, Nbatches+1)
    train_loss = list()
    test_loss = list()
    param_list = list()
    early_stop = False
    blow_up = False
    optimizer = optim.SGD(net.parameters(), lr=lr, momentum=.9)

    for epoch in range(nepoch):

        # if weights have blown up then don't do anymore work
        if blow_up:
            break

        dat_order = random.sample(batch_list, len(batch_list))
        iteration += 1

        if (epoch % loss_step == 0):
            train_l = compute_loss(X, Y, net, criterion, lam,
                                           update_type, p, lag)
            test_l = compute_loss(Xtest, Ytest, net, criterion, 0.,
                                          update_type, p, lag)
            train_loss.append(train_l)
            test_loss.append(test_l)
            if verbose > 0:
                print("----------------------------")
                print("epoch: ", epoch)
                print("training loss: ", train_l)
                print("test loss: ", test_l)
                print("----------------------------")

           # if epoch > 0 and test_l >= test_loss[-2]: 
           #     if verbose > 0:
           #         print("=> STOPPING: test error increased")
           #     early_stop = True
           #     break
            
            if not np.isfinite(test_l):
                if verbose > 0:
                    print("=> STOPPING: test error not finite")
                blow_up = True
                break

        for j in range(len(dat_order)):
            batchnum = dat_order[j]
            start_ind = (batchnum - 1)*mbatch
            end_ind = batchnum*mbatch
            inp_torch = torch.from_numpy(np.ascontiguousarray(X[:,start_ind:end_ind].T))

            inp = Variable(inp_torch.float())
            output = net(inp)
            target = torch.from_numpy(Y[start_ind:end_ind]).float()
            target = Variable(target)

            loss = criterion(output, target)
            if (opt_type == "subgradient"):
                loss += compute_torch_penalty_loss(net, update_type, p, lag, lam)

            net.zero_grad()
            loss.backward()

            optimizer.step()

            if (opt_type == "proximal"):
                param = list(net.parameters())
                update_weight = proximal_update(param[0].data.numpy(), lag, p,
                                                lam, update_type)
                param[0].data.sub_(torch.from_numpy(update_weight).float())
            
            # save the first layer weights for this iter
            param_list.append(list(net.parameters())[0].data.numpy().copy())

            if not np.all(np.isfinite(param_list[-1])):
                print("=> STOPPING: weights not finite")
                blow_up = True
                break

    if verbose > 0:
        print('Finished Training')

    # compute test error
    if not blow_up:
        if early_stop:
            param_0 = param_list[-2]
            train_loss = np.array(train_loss[:-1])[np.isfinite(train_loss)[:-1]]
            test_loss = np.array(test_loss[:-1])[np.isfinite(test_loss)[:-1]]
        else:
            param_0 = list(net.parameters())[0].data.numpy()
            train_loss = np.array(train_loss)[np.isfinite(train_loss)]
            test_loss = np.array(test_loss)[np.isfinite(test_loss)]
    else:
        param_0 = None
        test_loss = None
        train_loss = None

    return param_0, test_loss, train_loss
