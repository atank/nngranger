from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import os
import os.path as op
import pyprind
import shutil
import sys
import time
import torch

from itertools import product

from generate_data import (generate_data_general,
                           convert_ts_data_to_standard_training_format)
from nn_training_funcs import optimize_nn

parser = argparse.ArgumentParser()
parser.add_argument('--T', type=int, default=1000,
                    help='length of generated time series')
parser.add_argument('--arch', type=int, default=1,
                    help='network architecture')
parser.add_argument('--p', type=int, default=10,
                    help='number of time series')
parser.add_argument('--lag', type=int, default=1,
                    help='lag of generated data')
parser.add_argument('--sparsity', type=float, default=0.3,
                    help='sparsity fraction of generated data')
parser.add_argument('--lag_train', type=int, default=1,
                    help='maximum lags of model')
parser.add_argument('--pind', type=int, default=None,
                    help='series index to determing granger causality for')
parser.add_argument('--lam', type=float, default=1.,
                    help='regularization parameter for weight penalty')
parser.add_argument('--lr', type=float, action='append',
                    help='learning rate')
parser.add_argument('--nepoch', type=int, default=1000,
                    help='number of epochs')
parser.add_argument('--mbsize', type=int, default=None,
                    help='number of time points in minibatch')
parser.add_argument('--penalty_type', type=str, default="group_lasso",
                    help='type of weight penalty. [group_lasso, h_truncation]')
parser.add_argument('--opt_type', type=str, default='proximal',
                    help='type of optimization [proximal, subgradient]')
parser.add_argument('--seed', type=int, default=8675309,
                    help='random seed')
parser.add_argument('--nrestarts', type=int, default=1,
                    help='number of random restarts for optimization')
parser.add_argument('--outdir', type=str, default="./out/exp-mlp-var",
                    help='directory to store results')
parser.add_argument('--chkpointdir', type=str, default="./checkpoints/exp-mlp-var",
                    help='directory to store checkpoint files')
parser.add_argument('--verbose', type=int, default=0,
                    help='verbosity level')

args = parser.parse_args()

T = args.T
p = args.p
lag = args.lag
sparsity = args.sparsity
lag_train = args.lag_train
pind = args.pind
lam = args.lam
penalty_type = args.penalty_type
opt_type = args.opt_type
seed = args.seed
nrestarts = args.nrestarts
nepoch = args.nepoch
mbsize = args.mbsize

lr_lst = args.lr
if len(lr_lst) == 0:
    raise ValueError('must specify at least one learning rate')

OUTDIR = args.outdir
chkpointdir = args.chkpointdir

verbose = args.verbose

# fixed parameters
sd_beta = 1.
sd_x = 1.
sd_e = .1

# generaate synthetic data
Y_orig, beta, GC = generate_data_general("VAR", sparsity, p, sd_beta, sd_e, T, lag)

if mbsize is None:
    # do this once out here to get the minibatch size
    X, _, _, _ = convert_ts_data_to_standard_training_format(Y_orig, 0,
                                                             lag_train)
    mbsize = X.shape[1]

exp_name = "T-%d_p-%d_lag-%d_sp-%e_lagtrain-%d_pind-%d_lam-%e_"
exp_name += "_pen-%s_opt-%s_nepoc-%d_mbsz-%d_seed-%d"
exp_name = exp_name % (T, p, lag, sparsity, lag_train, pind,
                       lam, penalty_type, opt_type, nepoch, mbsize, seed)

#if not op.exists(OUTDIR):
#    os.makedirs(OUTDIR)

if not op.exists(op.join(chkpointdir, exp_name)):
    os.makedirs(op.join(chkpointdir, exp_name))

# if there are already results for this script don't reprocess
if op.isfile(op.join(OUTDIR, exp_name + ".pth.tar")):
    print("experiment already exists...skipping")
    sys.exit()


# set nn architectures to use here
# first entry is input size (usually lag_train)
# last entry is output size (always 1 for this model type)
if args.arch == 1:
    dims = [lag_train, lag_train, 1]
if args.arch == 2:
    dims = [lag_train, lag_train, lag_train, 1]

print("experiment: %s" % exp_name)
    
best_test_loss = np.inf
best_lr = lr_lst[0]
best_res = (None, None, None)

for lr, restart in product(lr_lst, range(nrestarts)):

    print("=> processing lr %e restart %d" % (lr, restart))
    chkpointfile = op.join(chkpointdir, exp_name, "checkpoint-%e-%d.pth.tar"
                                                   % (lr, restart))
    if op.isfile(chkpointfile):
        print("=> found checkpoint lr %e restart %d, skipping" % restart)
        res_dict = torch.load(chkpointfile)
        weights = res_dict['weights']
        train_loss = res_dict['train_loss']
        test_loss = res_dict['test_loss']
        if test_loss[-1] < best_test_loss:
            best_test_loss = test_loss[-1]
            best_res = (weights, test_loss, train_loss)
        continue

    # convert time series data into input-output format
    X, Y, Xtest, Ytest = convert_ts_data_to_standard_training_format(Y_orig, pind,
                                                                     lag_train)

    weights, test_loss, train_loss = optimize_nn(X, Y, Xtest, Ytest, lag_train, p,
                                                penalty_type, lam, nepoch=nepoch,
                                                mbatch=mbsize, lr=lr,
                                                opt_type=opt_type,
                                                loss_step=100,
                                                verbose=verbose)

    # if returned values are None then algorithm did not converge
    if (weights is not None and test_loss is not None
            and train_loss is not None):

        if test_loss[-1] < best_test_loss:
            best_test_loss = test_loss[-1]
            best_res = (weights, test_loss, train_loss)
            best_lr = lr

    chkdict = {'weights': weights, 'train_loss': train_loss,
               'test_loss': test_loss}
    torch.save(chkdict, chkpointfile)


# save results -- even if nan (they'll be thrown out later)
resfile = op.join(OUTDIR, exp_name + ".pth.tar")

exper_par = {'T': T, 'p': p, 'pind': pind, 'lag': lag, 'sparsity': sparsity,
             'seed': seed}
alg_par = {'lag_train': lag_train, 'lam': lam, 'lr': best_lr,
           'penalty_type': penalty_type, 'opt_type': opt_type,
           'nepoch': nepoch, 'mbsize': mbsize
          }

weights, test_loss, train_loss = best_res
out_dict = {'weights': weights,
            'train_loss': train_loss,
            'test_loss': test_loss,
            'GC': GC[pind],
            'exper_par': exper_par,
            'alg_par': alg_par
            } 
torch.save(out_dict, resfile)
    
shutil.rmtree(op.join(chkpointdir, exp_name))
