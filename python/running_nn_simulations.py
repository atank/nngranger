from __future__ import division

import matplotlib.pyplot as plt
import numpy as np

from generate_data import generate_data_general, convert_ts_data_to_standard_training_format
from nn_training_funcs import optimize_nn

data_type = "VAR"
penalty_type = "h_truncation"
# lag of generated data
lag = 1
p = 10
mbatch_size = 10
lr_list = [.01]
lam_list = [.00001,.0001,.001,.01]
N = 1000
sparsity = .3
sd_beta = 1
sd_x = 1
sd_e = .1
lag = 1
lam = .001
np.random.seed(12321)
# lag the model uses
lag_train=3

###generate simulation data for NN model
Y,beta,GC = generate_data_general(data_type,sparsity,p,sd_beta,sd_e,N,lag)

##convert TS data into standard input-output format
p_i = 3 ##GC index
X,Y,Xtest,Ytest = convert_ts_data_to_standard_training_format(Y,p_i,lag_train)

lr = 0.01
nepoch = 10000
opt_type = "proximal"
params,test_loss,train_loss = optimize_nn(X,Y,Xtest,Ytest,lag_train,p,penalty_type,lam,nepoch=nepoch,lr=lr,opt_type=opt_type)

#test_prob,train_prob_list,params = cross_validation_nn(X,Y,Xtest,Ytest,lag,p,penalty_type,lam_list,lr_list,nepoch=500000,opt_type="proximal")

#gamma = np.linalg.norm(params, axis=0)
#print((gamma > 0.001)*1)
GC_est = np.zeros_like(GC)


Trunc_est = np.zeros((p,lag_train))
for i in range(p):
   for j in range(lag_train):
       gamma = np.linalg.norm(params[:,(i*lag_train + j)])
       Trunc_est[i,j] = (gamma > 0).astype(np.int)

for i in range(p):
    gamma = np.linalg.norm(params[:,i*lag_train:(i+1)*lag_train])
    GC_est[p_i,i] = (gamma > 0.001).astype(np.int)
print(gamma)
print("True GC")
print(GC[p_i,:])
print("estimated GC")
print(GC_est[p_i])

print(Trunc_est)

Trunc_true = np.zeros((p,lag_train))
Trunc_true[:,0] = GC[p_i,:]

#plt.plot(train_loss)
#plt.ylabel('training loss')
#plt.show()

#plt.pcolor(Trunc_est,cmap = 'Greys')
fig = plt.figure()
im = plt.imshow(np.transpose(Trunc_est),cmap='Greys',interpolation='none')
ax = plt.gca()
plt.xticks([],[])
plt.yticks([],[])
ax.set_yticks(np.arange(-.5, 3, 1), minor=True);
ax.set_xticks(np.arange(-.5, 10, 1), minor=True);
ax.axes.xaxis.set_ticklabels([])
ax.axes.yaxis.set_ticklabels([])
ax.grid(which='minor',color='c',linestyle='-',linewidth=2)
fname = "hest" + str(lam) + ".png"
fig.savefig(fname)

fig = plt.figure()
im = plt.imshow(np.transpose(Trunc_true),cmap='Greys',interpolation='none')
ax = plt.gca()
plt.xticks([],[])
plt.yticks([],[])
ax.set_yticks(np.arange(-.5,3,1),minor=True);
ax.set_xticks(np.arange(-.5,10,1),minor=True);
ax.axes.xaxis.set_ticklabels([])
ax.axes.yaxis.set_ticklabels([])
ax.grid(which='minor',color='c',linestyle='-',linewidth=2)
fname= "ht" + ".png"
fig.savefig(fname)

