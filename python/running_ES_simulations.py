#This file runs a prototypical experiment for determining Granger Causality using
#Multivariate Echo State Networks
from __future__ import division

import matplotlib.pyplot as plt
import numpy as np

from generate_data import generate_data_general, convert_ts_data_to_standard_training_format
from echo_state import echo_state_GC


data_type = "VAR"
lag = 1
p = 5
N = 100
sparsity = .3
sd_beta = 1
sd_x = 1
sd_e = .1
lag = 1
nlam = 7
n_hidden = 10
np.random.seed(12321)
test_frac = .8


###generate simulation data for NN model
Y,beta,GC = generate_data_general(data_type,sparsity,p,sd_beta,sd_e,N,lag)

gamma = echo_state_GC(Y,n_hidden,nlam,test_frac)
###convert TS data into standard input-output format




