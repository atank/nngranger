from __future__ import division

import numpy as np
from pyglmnet import GLM
from scipy.special import expit


"""
This function takes as input a multivariate time series and outputs
a Granger Causality graph based on an Echo State feature representation
of each series. 
"""

def echo_state_GC(X,n_hidden,lam,test_frac,nonlinear="logistic",opt_type="coordinate"):

    p,N = X.shape
    N_train = int(np.floor(N*test_frac))
    N_test = N - N_train
    X_train = X[:,range(N_train)]
    X_test = X[:,range(N_train,N)]

    if opt_type == "coordinate":
        Gamma = np.zeros((p,p))
    if opt_type == "LARS":
        Gamma = np.zeros((p,p,p))

    H = np.zeros((p,n_hidden,N))
    #first create an Echo State random feature representation of each series
    for i in range(p):
        H[i,:,:] = echo_state_feature_representation(X[i,:],n_hidden,nonlinear)

    group_idxs = np.zeros((n_hidden))
    for i in range(1,p):
        group_idxs = np.append(group_idxs,i*np.ones((n_hidden))) 

    #convert the output into a standard X,Y regression setting
    X_f = convert_to_regression(H) 
    #perfrom group lasso regression with CV to select the Granger causality graph
    for i in range(p):
        if opt_type == "coordinate":
            beta,Gamma[i,:],score = group_block_coordinate_reg(X[i,range(1,N_train)],X_f[:,range(1,N_train)],group_idxs,lam)
        if opt_type == "LARS":
            beta,Gamma[i,:,:],score = group_block_coordinate_reg(X[i,range(1,N_train)],X_f[:,range(1,N_train)],group_idxs,lam)

    return(Gamma)

"""""
This function takes as input a univariate time series and outputs an echo state feature
representation of the series. It also outputs the random parameters of the Echo State network
used to transform the series.
"""""

def echo_state_feature_representation(X,n_hidden,nonlinear,phi_u=1,a_u=.4,phi_w=1,a_w=.4,spec_radius=.8):
    ###first randomly generate the parameters of the model
    N = X.size
    U_sparsity = np.random.binomial(1,phi_u,n_hidden*n_hidden).reshape(n_hidden,n_hidden)
    U_mask = np.random.uniform(-a_u,a_u,n_hidden*n_hidden).reshape(n_hidden,n_hidden)
    U = U_sparsity*U_mask
    W_sparsity = np.random.binomial(1,phi_w,n_hidden)
    W_mask = np.random.uniform(-a_w,a_w)
    W = W_sparsity*W_mask
    H = np.zeros((n_hidden,N))


    for i in range(1,N):
        if (nonlinear == "tanh"):
            H[:,i] = np.tanh(np.dot(U,H[:,i-1]) + W*X[i-1])
        if (nonlinear == "logistic"):
            H[:,i] = expit(np.dot(U,H[:,i-1]) + W*X[i-1])

    #print(H)

    return(H)



"""""
This function converts the raw echo state features across series into a
form to be used in sparse group lasso regression.
"""""

def convert_to_regression(H):
    p,n_hidden,N = H.shape
    X = np.zeros((N,p*n_hidden))
    for i in range(N):
        X[i,:] = H[:,:,i].flatten() 

    return(np.transpose(X))

"""""
This function performs group lasso regression. 
"""""


def group_block_coordinate_reg(y,X,group_idxs,lam):

    lam = float(lam)
    tol = .001
    diff = 100
    beta0 = 0.0*group_idxs
    ngroups = np.unique(group_idxs).size
    ind_list = [0 for x in range(ngroups)]
    norm_xk = np.zeros((ngroups))
    for i in range(ngroups):
        here = np.where(group_idxs==i)[0]
        print(here)
        ind_list[i] = here
         #norm_xk[i] = np.norm(X[])

    #orthogonalize columns in each group
    X_n = 0.0*X
    for i in range(ngroups):
        U,s,V  = np.linalg.svd(X[ind_list[i],:],full_matrices=0)
        X_n[ind_list[i],:] = V


    R = 1.0*y
    itera = 0
    while (diff > tol):
        beta_old = 1*beta0
        itera = itera + 1
        for i in range(ngroups):
            Ri = R + np.dot(np.transpose(X_n[ind_list[i],:]),beta0[ind_list[i]])
            temp = np.dot(X_n[ind_list[i],:],Ri)
            norm_temp = np.linalg.norm(temp)
            beta0[ind_list[i]] = max(0.0,1.0-(lam/norm_temp))*temp
            R = Ri - np.dot(np.transpose(X_n[ind_list[i],:]),beta0[ind_list[i]])
        print(beta0)
        diff = np.linalg.norm(beta0 - beta_old)
    print(itera)
    gamma = np.zeros(ngroups)
    for i in range(ngroups):
        gamma[i] = np.linalg.norm(beta0[ind_list[i]])

    return(beta0,gamma)



"""
This algorithm implements the group lasso LARS algorithm describes in Yuan and Lin
JASA paper 2006. We assume all things have been standardized to mean zero unit variance
before hand :)
Input:
y: n x 1 response vector
X: covariates
group_idxs: group indicators for each covariate

output: 
beta: p x p set of covariates where the jth column is the beta for first j nonzero
groups
gamma: ngroups x ngroups where the jth column is the norm of each group of jth column of beta above
"""
def group_LARS(y,X,group_idxs,y_test,X_test):
    p = group_idxs.shape[0]
    ngroups = np.unique(group_idxs).shape[0]
    beta = np.zeros((p,ngroups+1))
    gamma_GC = np.zeros((ngroups,ngroups+1))

    group_list = range(ngroups)
    r = 1.0*y
    ind_list = [0 for x in range(ngroups)]
    num_group = np.zeros(ngroups)
    for i in range(ngroups):
        here = np.where(group_idxs==i)[0]
        #print(here)
        ind_list[i] = here
        num_group[i] = here.shape[0]

    print(ind_list)
    #print(num_group)
    ##step 2 - compute most correlated set
    corr = np.zeros(ngroups)
    for j in range(ngroups):
        corr[j] = np.linalg.norm(np.dot(X[ind_list[j],:],r))/np.sqrt(num_group[j])
    jstar = np.argmax(corr)
    ind_include = ind_list[jstar]
    groups_not_in = np.setdiff1d(range(ngroups),jstar)
    k = 0
    while k < ngroups:
        print(k)
        k = k + 1
        #perform regression on residuals to get LARS direction
        #step 3
        gamma = np.zeros(p)
        gamma[ind_include] = np.dot(np.linalg.inv(np.dot(X[ind_include,:],np.transpose(X[ind_include,:]))), np.dot(X[ind_include,:],r))
        #step 4
        alpha = np.zeros(groups_not_in.shape[0])
        X_gamma = np.dot(np.transpose(X),gamma)
        XjstarXjstar = np.dot(np.transpose(X[ind_list[jstar],:]),X[ind_list[jstar],:])
        for j in groups_not_in:
            ###solve alpha
            XjXj = np.dot(np.transpose(X[ind_list[j],:]),X[ind_list[j],:])
            temp = (XjXj/num_group[j]) - (XjstarXjstar/num_group[jstar])
            a = np.dot(np.dot(np.transpose(X_gamma),temp),X_gamma)
            b = -2*np.dot(np.dot(np.transpose(X_gamma),temp),r)
            c = np.dot(np.dot(r,temp),r)

            alpha1 = (-b + np.sqrt(b*b - 4*a*c))/(2*a)
            alpha2 = (-b - np.sqrt(b*b - 4*a*c))/(2*a)
            if (0 <= alpha1 <= 1):
                alpha[j] = alpha1
            if (0 <= alpha2 <= 1):
                alpha[j] = alpha2

        if groups_not_in.shape[0] > 0:
            jstar = groups_not_in[np.argmax(-alpha)]
            alpha_temp = alpha[jstar]
            ind_include = np.append(ind_include,ind_list[jstar])
            groups_not_in = np.setdiff1d(groups_not_in,jstar)
        else:
            alpha_temp = 1

        beta[:,k] = beta[:,k-1] + alpha_temp*gamma
        r = y - np.dot(np.transpose(X),beta[:,k]) 

        for i in range(ngroups):
            gamma_GC[i,k] = np.linalg.norm(beta[ind_list[i],k])

        ####perform prediction on a test set for all inferred beta values####
        test_error = np.zeros(ngroups+1)
        for i in range(ngroups+1):
            temp = y_test - np.dot(np.transpose(X_test),beta[:,i])
            test_error[i] = sum(temp*temp) 


    return(gamma_GC,beta,test_error)




def group_lasso_regression(p,y_train,X_train,y_test, X_test,group_idxs,reg_lambda):

    #x = 10
    print(reg_lambda)
    gl_glm = GLM(distr="gaussian",
             tol=1e-5,
             group=group_idxs,
             score_metric="pseudo_R2",
             alpha=1.0,
             reg_lambda=reg_lambda)


    #print(group_idxs.shape)
    #print(X_train.shape)
    #print(np.isnan(X_train).any())
    print(reg_lambda)
    gl_glm.fit(np.transpose(X_train),y_train)



    nlam = len(reg_lambda)
    scores = gl_glm.score(np.transpose(X_test),y_test)
    #determine the total 'norm' of the weights for each group
    Gamma_GC = np.zeros((p,nlam))
    for j in range(nlam):
        betas = gl_glm.fit_[j]['beta']
        print(betas.size)
        print(group_idxs.size)
        for i in range(p):
            Gamma_GC[i,j] = np.linalg.norm(betas[np.where(group_idxs == i)[0]])

    return(Gamma_GC,scores)



if __name__ == "__main__": 
    np.random.seed(18721)
    Ntrain = 30
    Ntest = 10000
    N = Ntrain + Ntest
    p = 15
    beta_true = np.array([0,0,0,0,0,0,0,0,0,0,1,1,1,1,1])
    X = np.random.randn(N,p)
    y = np.dot(X,beta_true) + .01*np.random.randn(N)
    group_idxs = 1*beta_true
    Xtrain = X[range(0,Ntrain),:]
    Xtest = X[range(Ntrain,N),:]
    ytrain = y[range(0,Ntrain)]
    ytest = y[range(Ntrain,N)]
    #beta, gamma = group_block_coordinate_reg(y,np.transpose(X),group_idxs,1)
    gamma_GC,beta, test_error = group_LARS(ytrain,np.transpose(Xtrain),group_idxs,ytest,np.transpose(Xtest))


    

