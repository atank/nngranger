from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np


def tpr_fpr(Gamma, GC_true, thresh):
    """ Compute the true positive rate and the false positive rate 

    inputs:

    Gamma : ndarray, (p, p)
        estimated connectivity matrix where all values are >= 0
    GC_true : ndarray, (p, p)
        true connectivity matrix where all elements are 0 or 1
    thresh : float
        threshold parameter for Gamma connectivity matrix
    
    output:

    tp_rate : float
        true positive rate
    fp_rate : float
        false positive rate

    """
    GC_est = (Gamma > thresh)*1
    est_on = np.where(GC_est.flatten() == 1)[0]
    true_on = np.where(GC_true.flatten() == 1)[0]

    #print(np.intersect1d(est_on,true_on))
    tp_rate = np.intersect1d(est_on,true_on).size/true_on.size

    true_not_on = np.where(GC_true.flatten() == 0)[0]

    if len(true_not_on) > 0:
        fp_rate = np.intersect1d(est_on,true_not_on).size/true_not_on.size
    else:
        fp_rate = 0. 

    return(tp_rate,fp_rate)


def calculate_AUC(true_positive_rate, false_positive_rate):
    """ Compute the area under the ROC curve for a set of true- and
        false-positive rates.
    
        inputs:

        true_positive_rate : length k array of doubles.
            should be ordered with respect to the false_positive_rate

        false_positive_rate : length k array of doubles.
            should be ordered from smallest to largest 
        
        outputs: the area under the ROC curve (AUC) (double)
    """
    tpr = np.empty(len(true_positive_rate)+2)
    tpr[0] = 0.
    tpr[-1] = 1.
    tpr[1:-1] = true_positive_rate
    #true_positive_rate = np.concatenate(([0],true_positive_rate))
    #true_positive_rate = np.concatenate((true_positive_rate,[1]))

    fpr = np.empty(len(false_positive_rate)+2)
    fpr[0] = 0.
    fpr[-1] = 1.
    fpr[1:-1] = false_positive_rate
    #false_positive_rate = np.concatenate(([0],false_positive_rate))
    #false_positive_rate = np.concatenate((false_positive_rate,[1]))

    return np.trapz(tpr, fpr)
