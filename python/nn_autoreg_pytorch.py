from __future__ import division
import torch
from torch.autograd import Variable
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F


class LSTM_realout(nn.Module):

    def __init__(self, p, hidden_dim):
        super(LSTM_realout, self).__init__()
        self.hidden_dim = hidden_dim


        # The LSTM takes word embeddings as inputs, and outputs hidden states
        # with dimensionality hidden_dim.
        self.lstm = nn.LSTM(p, hidden_dim)

        # The linear layer that maps from hidden state space to tag space
        self.out = nn.Linear(hidden_dim, 1)
        



    def init_hidden(self):
        # Before we've done anything, we dont have any hidden state.
        # Refer to the Pytorch documentation to see exactly
        # why they have this dimensionality.
        # The axes semantics are (num_layers, minibatch_size, hidden_dim)
        return (autograd.Variable(torch.zeros(1, 1, self.hidden_dim)),
                autograd.Variable(torch.zeros(1, 1, self.hidden_dim)))

    def forward(self, input,hidden):
        lstm_out, hidden = self.lstm(input, hidden)
        #self.hidden = self.repackage_hidden(self.hidden)
        #print(lstm_out)
        #print(self.hidden)
        output = self.out(lstm_out.view(len(lstm_out),-1))
        return output, hidden


class RNN_Elman(nn.Module):

    def __init__(self,input_size,hidden_size,output_size,shared_output=False):
        super(RNN_Elman, self).__init__()
        # an affine operation: y = Wx + b
        self.hidden_size = hidden_size
        self.p = input_size
        self.hidden_size_total = hidden_size*input_size
        self.i2h = nn.Linear(input_size + self.hidden_size_total, self.hidden_size_total)
        if shared_output:
            self.i2o = nn.Linear(hidden_size,1)
        else:
            self.i2o = nn.Linear(self.hidden_size_total,input_size)

        self.sigmoid = nn.Sigmoid()
        self.shared_output = shared_output
        #self.fc3 = nn.Linear(84, 10)

    def forward(self,input,hidden):
        combined = torch.cat((input, hidden), 1)
        hidden = self.sigmoid(self.i2h(combined))
        # #compute output for each individual thing...
        dummy_holder_list = []
        for i in range(p):
            dmh = Variable(torch.zeros(self.hidden_size_total,self.hidden_size_total))
            index_start = i*self.hidden_size
            index_end = (i + 1)*self.hidden_size
            dmh[index_start:index_end,index_start:index_end] = torch.eye(self.hidden_size)
            dummy_holder_list.append(dmh)

        #dummy_holder = Variable(torch.zeros(self.hidden_size_total,self.hidden_size_total))

        output_list = Variable(torch.zeros(1,self.p))
        for i in range(p):
            #index_start = i*self.hidden_size
            #index_end = (i + 1)*self.hidden_size
            #dummy_holder[index_start:index_end,index_start:index_end] = torch.eye(self.hidden_size)
        #     print(hidden)
        #     print(dummy_holder)
            temp = self.i2o(torch.t(dummy_holder_list[i].mm(torch.t(hidden))))
        #     #print(temp)

            output_list[0,i] = temp[0,i].clone() 
            #dummy_holder[index_start:index_end,index_start:index_end] = torch.zeros(self.hidden_size,self.hidden_size)

        # #x = self.fc3(x)
        return output_list, hidden

    def initHidden(self):
        return Variable(torch.zeros(1, self.hidden_size_total))


p = 3
n_hidden = 3
criterion = nn.MSELoss()
net = LSTM_realout(p,n_hidden*p)
input = Variable(torch.randn(1,p))
hidden = net.init_hidden()
output,hidden = net(input.view(1,1,-1),hidden)
target = Variable(torch.ones(1,1))
loss = criterion(output,target)

net.zero_grad()
loss.backward()

#ndim = 20
#lag = 1
#net = Net(ndim,lag)
#print(net)
 
#criterion = nn.MSELoss()

#nepoch = 10









